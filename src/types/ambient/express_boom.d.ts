type ErrorHandler = (message?: string, data?: any) => any;

declare module 'express-boom';

declare namespace Express {
    interface Boom {
      badImplementation: ErrorHandler;
      badRequest: ErrorHandler;
      conflict: ErrorHandler;
      notFound: ErrorHandler;
      unauthorized: ErrorHandler;
      forbidden: ErrorHandler;
    }

    export interface Response {
        boom: Boom;
    }
}
