import * as Chai from 'chai';
import { expect } from 'chai';
import * as uuid from 'uuid/v4';

import RBAC from '../../../src/lib/auth/rbac';
import standardRoles from '../../../src/lib/auth/roles';

const rbac = new RBAC(standardRoles);
const role = 'admin';

describe('Permissions: Contributor Role', function() {
  describe('Games Permissions', function() {
    it('should allow an admin to see games: game:read', done => {
      rbac
        .can(role, 'game:read', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to create games: game:create', done => {
      rbac
        .can(role, 'game:create', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to update games: game:update', done => {
      rbac
        .can(role, 'game:update', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should fail to let an admin to delete a game: game:delete', done => {
      rbac
        .can(role, 'game:delete', {})
        .catch(error => {
          expect(error).to.equal(false);
          done();
        });
    });
  });

  describe('Users Permissions', function() {
    it('should allow an admin to see other users: user:read', done => {
      rbac
        .can(role, 'user:read', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to update themselves: user:update', done => {
      const userId = uuid();

      rbac
        .can(role, 'user:update', {
          user: {
            _id: userId,
            is_active: true,
          },
          requestor: { _id: userId }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to update a different user: user:update', done => {
      rbac
        .can(role, 'user:update', {
          user: {
            _id: uuid(),
            is_active: true,
          },
          requestor: { _id: uuid() }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should fail to let an admin delete anyone including themselves: user:delete', done => {
      rbac
        .can(role, 'user:delete', {})
        .catch(result => {
          expect(result).to.equal(false);
          done();
        });
    });
  });

  describe('Groups Permissions', function() {
    it('should allow an admin to create groups: group:create', done => {
      rbac
        .can(role, 'group:create', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to update a group they own it or are not a member of the group: group:update', done => {
      const userId = uuid();

      rbac
        .can(role, 'group:update', {
          requestor: { _id: userId },
          group: { owner: userId }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to see a group they\'re not a member of', done => {
      rbac
        .can(role, 'group:read', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to delete a group they own: group:delete', done => {
      const userId = uuid();

      rbac
        .can(role, 'group:delete', {
          requestor: { _id: userId },
          group: { owner: userId }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should fail to let an admin to delete a group they don\'t own: group:delete', done => {
      rbac
        .can(role, 'group:delete', {
          requestor: { _id: uuid() },
          group: { owner: uuid() }
        })
        .catch(result => {
          expect(result).to.equal(false);
          done();
        });
    });
  });

  describe('Events Permissions', function() {
    it('should allow an admin to create events: event:create', done => {
      rbac
        .can(role, 'event:create', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should allow an admin to see an event that is public: event:read', done => {
      const userId = uuid();

      rbac
        .can(role, 'event:read', {
          event: { is_public: true }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    xit('should allow an admin to see events they don\'t own or are a attendee of: event:read', done => {
      rbac
        .can(role, 'event:read', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    xit('should allow an admin to see an event that is private they they are invited to: event:read', done => {
      const userId = uuid();

      rbac
        .can(role, 'event:read', {
          requestor: { _id: userId },
          event: {
            is_public: false,
            attendees: [uuid(), uuid(), uuid()],
          }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    xit('should allow an admin updatea any event: event:update', done => {
      rbac
        .can(role, 'event:update', {
          requestor: { _id: uuid() },
          event: { owner: uuid() }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });
  });

  describe('Play Permissions', function() {
    it('should allow an admin to create a play entry: play:create', done => {
      rbac
        .can(role, 'play:create', {})
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    xit('should allow an admin to create a play entry: play:read');

    it('should allow an admin to update a play entry if they created it: play:update', done => {
      const userId = uuid();

      rbac
        .can(role, 'play:update', {
          requestor: { _id: userId },
          play: { owner: userId }
        })
        .then(result => {
          expect(result).to.equal(true);
          done();
        });
    });

    it('should fail to let an admin update a play entry if they did\'t created it: play:update', done => {
      rbac
        .can(role, 'play:update', {
          requestor: { _id: uuid() },
          play: { owner: uuid() }
        })
        .catch(result => {
          expect(result).to.equal(false);
          done();
        });
    });

  });
});
