export default {
  superadmin: {
    inherits: ['admin'],
    can: [
      'user:create',
      'user:delete',
      'game:delete',
      'play:delete'
    ]
  },
  admin: {
    inherits: ['contributor'],
    can: [
      'user:update',
      'group:read',
      'group:update',
      'event:read',
      'event:update',
      'play:read'
    ]
  },
  contributor: {
    inherits: ['user'],
    can: [
      'game:create',
      'game:update'
    ]
  },
  user: {
    can: [
      'game:read',
      'user:read',
      {
        name: 'user:update',
        when: (params: any) => {
          return (
            params.user.is_active &&
            params.requestor._id.toString() === params.user._id.toString()
          );
        }
      },
      {
        name: 'user:create',
        when: (params: any) => {
          switch (params.payload.role) {
            case 'admin':
            case 'superadmin':
              return params.requestor.role === 'superadmin';
            case 'contributor':
              return params.requestor.role === 'admin' || params.requestor.role === 'superadmin';
            default:
              return false;
          }
        }
      },
      {
        name: 'group:read',
        when: (params: any) => {
          return (
            params.group.is_public ||
            params.group.members.find((id: string) => (
              params.requestor._id.toString() === id.toString()
            ))
          );
        }
      },
      'group:create',
      {
        name: 'group:update',
        when: (params: any) => {
          return params.requestor._id.toString() === params.group.owner.toString();
        }
      },
      {
        name: 'group:delete',
        when: (params: any) => {
          return params.requestor._id.valueOf() === params.group.owner;
        }
      },
      'event:create',
      {
        name: 'event:read',
        when: (params: any) => {
          return (
            params.event.is_public ||
            !!params.group.attendees.find((id: string) => params.requestor._id) ||
            !!params.group.invitees.find((id: string) => params.requestor._id)
          );
        }
      },
      {
        name: 'event:update',
        when: (params: any) => {
          return params.requestor._id.toString() === params.event.owner.toString();
        }
      },
      {
        name: 'event:delete',
        when: (params: any) => {
          return params.currentUser._id === params.event.owner;
        }
      },
      'play:create',
      {
        name: 'play:read',
        when: (params: any) => {
          return !!params.play.players.find((id: string) => params.requestor._id);
        }
      },
      {
        name: 'play:update',
        when: (params: any) => {
          return params.requestor._id === params.play.owner;
        }
      }
    ]
  }
};
