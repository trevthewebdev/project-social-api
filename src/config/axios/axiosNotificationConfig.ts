import * as axios from 'axios';
import config from '../index';

const instance = axios.create({
  baseURL: 'http://localhost:4000'
});

export default instance;
