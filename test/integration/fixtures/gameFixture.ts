export const parent = {
  _id: '67bfb242496f58681423d2d6',
  name: 'Suburbia',
  player_min: 2,
  player_max: 4,
  player_best: 3,
  player_interaction: ['competitive']
};

export const expansion = {
  _id: '5934687f584a80188ca44944',
  name: 'Five Star',
  is_expansion: true,
  parent_game: '67bfb242496f58681423d2d6',
  player_interaction: ['competitive']
};
