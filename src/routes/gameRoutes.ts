import * as express from 'express';

import { GAME_FILTER_MAP } from '../controllers/data/gamefilterMap';
import { validateRequest } from '../validators/shared/customValidators';
import { GameCreateSchema, GameUpdateSchema, GameListingRequestSchema } from '../validators/gameValidators';
import authMiddleware from '../middleware/authMiddleware';
import { prepIncludes, prepSparseFeilds } from '../middleware/request/queryParamsMiddleware';
import { setFilters } from '../middleware/request';
import { idOrSlug } from '../middleware/requestMiddleware';
import {
  get,
  getOne,
  post,
  update,
  remove
} from './../controllers/gameController';

const Router = express.Router();
const auth = authMiddleware();
import { gauth } from '../config/auth';

Router.use(auth.authenticate());

// All Games
Router.get(
  '/',
  validateRequest(GameListingRequestSchema, 'query'),
  prepIncludes,
  prepSparseFeilds,
  setFilters(GAME_FILTER_MAP),
  get
);

Router.get('/:id', idOrSlug, getOne);
Router.post('/', gauth.permit('game:create'), validateRequest(GameCreateSchema), post);
Router.patch('/:id', gauth.permit('game:update'), validateRequest(GameUpdateSchema), update);
Router.delete('/:id', gauth.permit('game:delete'), remove);

export default Router;
