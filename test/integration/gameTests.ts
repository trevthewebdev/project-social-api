import * as Chai from 'chai';
import { expect } from 'chai';
const chaiHttp = require('chai-http');

import { authenticateTestUser } from './utils/authTestUtils';
import server from './../../src/server';
import { IGameDetails } from './../../src/types/gameTypes';
import { parent, expansion } from './fixtures/gameFixture';
import {
  GameDetailsSchema
} from './../../src/validators/gameValidators';
import { staticUsers } from '../../seed/data/userSeedData';

Chai.use(chaiHttp);

const Agent = Chai.request.agent(server);
const superAdminUser = staticUsers.find(({ role }) => role === 'superadmin');

describe('GET: Games', () => {
  let superAdminToken: string;

  before(function(done: (error?: any) => void) {
    authenticateTestUser(Agent, superAdminUser)
      .then(token => {
        superAdminToken = token;
      })
      .catch(error => done(error));
  });

  // describe('Game Listing, filtering, & sorting', () => {
  //   it('should return a list of games using default settings', done => {
  //     api
  //       .get('/v1/games', {
  //         headers: { Authorization: `Bearer ${superAdminToken}` }
  //       })
  //       .then((response: any) => {
  //         const { data, meta } = response.data;
  //         const sample = _.sample(data);

  //         expect(response).to.have.property('status', 200);
  //         expect(_.isArray(data)).to.equal(true);

  //         expect(data).to.have.length(20);
  //         expect(meta).to.equal({
  //           total: 75,
  //           skip: 0,
  //           limit: 20
  //         });

  //         expect(sample).to.have.property('name');
  //         expect(sample).to.have.property('slug');
  //         expect(sample).to.have.property('min_duration');
  //         expect(sample).to.have.property('max_duration');
  //         expect(sample).to.have.property('player_min');
  //         expect(sample).to.have.property('player_max');
  //         done();
  //       });
  //   });

  //   it('should return a list of games owned by a specific user');
  //   it('should return a list of games with a min duration');
  //   it('should return a list of games with a max duration');
  //   it('should return a list of games with min/max duration range');
  //   it('should return a list of games with a min player count');
  //   it('should return a list of games with a max player count');
  //   it('should return a list of games with a specific primary mechanic');
  //   it('should return an empty array with correct meta data if no game was found');
  // });

  // describe('Getting Individual Games', () => {
  //   it('should return a single game by :id');
  //   it('should return a single game by :slug');
  //   it('should fail to return a game if it\'s not found');
  // });

  // Adding
  describe('Adding Board Games: POST', () => {
    it('should ADD a game: POST /api/v1/games', (done) => {
      Agent
        .post('/api/v1/games')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(parent)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }

          const validation = GameDetailsSchema.validate(response.body.data);

          expect(response).to.have.status(201);
          expect(validation.error).to.be.null;
          done();
        });
    });

    it('should ADD an expansion game & update the parent game: POST /api/v1/games', (done) => {
      Agent
        .post('/api/v1/games')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(expansion)
        .then(() => {
          return (
            Agent
              .get(`/api/v1/games/${parent._id}`)
              .set('Authorization', `Bearer ${superAdminToken}`)
              .then((response: any) => {
                expect(response.body.data.expansions).to.include(expansion._id);
                done();
              })
              .catch((err) => {
                console.error(err);
                throw err;
              })
          );
        })
        .catch(err => {
          console.error(err);
          throw err;
        });
    });

    it('should FAIL to ADD a game with a CONFLICT: POST /api/v1/games', (done) => {
      Agent
        .post('/api/v1/games')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(parent)
        .end((err: Error, response: any) => {
          expect(err).to.have.status(409);
          done();
        });
    });

    it('should FAIL to ADD a game with invalid payload: POST /api/v1/games', (done) => {
      const modifiedParent = Object.assign({}, parent);
      delete modifiedParent.name;

      Agent
        .post('/api/v1/games')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(modifiedParent)
        .end((err: Error, response: any) => {
          expect(err).to.have.status(400);
          done();
        });
    });
  });

  // Getting
  describe('Getting Board Games: GET', () => {
    it('should RETURN all games: /api/v1/games/', (done) => {
      Agent
        .get('/api/v1/games')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }
          const listing = response.body.data;
          const validation = GameDetailsSchema.validate(listing[0]);

          expect(response).to.have.status(200);
          expect(listing).to.be.an('array');
          expect(listing.length).to.be.above(0);
          expect(validation.error).to.be.null;
          done();
        });
    });

    it('should RETURN a SINGLE game by ID: /api/v1/games/:id', (done) => {
      Agent
        .get(`/api/v1/games/${parent._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: any, response: any) => {
          if (err) {
            return done(err);
          }
          expect(response).to.have.status(200);
          expect(response.body.data.name).to.equal(parent.name);
          done();
        });
    });

    it('should RETURN a SINGLE game by SLUG: /api/v1/games/:id', (done) => {
      Agent
        .get(`/api/v1/games/suburbia-five-star`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }
          expect(response).to.have.status(200);
          expect(response.body.data.slug).to.equal('suburbia-five-star');
          done();
        });
    });
  });

  // Updating
  describe('Updating Board Games: PATCH', () => {
    it('should update a SINGLE game on /api/v1/games/:id', (done) => {
      Agent
        .patch(`/api/v1/games/${parent._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send({
          name: 'Battlelore',
          player_min: 2,
          player_max: 2
        })
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }
          expect(response).to.have.status(204);
          done();
        });
    });

    it('should FAIL to UPDATE a SINGLE game with invalid id param: PATCH /api/v1/games/:id', (done) => {
      Agent
        .patch(`/api/v1/games/123ese`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send({ name: 'Suburbia' })
        .end((err: Error, response: any) => {
          expect(err).to.have.status(500);
          done();
        });
    });
  });

  // Removing
  describe('Removing Board Games: DELETE', () => {
    it('should delete a SINGLE game on /api/v1/games/:id', (done) => {
      Agent
        .del(`/api/v1/games/${expansion._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }
          expect(response).to.have.status(204);
          done();
        });
    });
  });

});
