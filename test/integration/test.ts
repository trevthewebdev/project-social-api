import 'mocha';
import log from '../../src/utils/logUtils';
import cleanAndSeedDB from './../../seed/seed';

before(function(done: (error?: any) => void) {
  this.timeout(20000);

  cleanAndSeedDB(false)
    .then(() => {
      log.info('Database ready for tests\n');
      done();
    })
    .catch((err: Error) => {
      log.info('Something went wrong when cleaning the database', err);
    });
});

import './auth/authentication.test';
// import './auth/permissions.test';

import './userTests';
// import './users/getUsers.test';
// import './users/createUsers.test';
// import './users/updateUsers.test';
// import './users/deleteUsers.test';

// games
import './games/getGames.test';
// import './games/createGames.test';
// import './games/updateGames.test';
// import './games/deleteGames.test';

// events
// import './events/createEvents.test';

import './groupTests';
import './locationTests';

// unit tests
import '../unit/rbac/userRbac.test';
import '../unit/rbac/contributorRbac.test';
import '../unit/rbac/adminRbac.test';
// import '../unit/rbac/superadminRbac.test';
