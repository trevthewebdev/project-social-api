import * as Faker from 'faker';
import { IUserDetails } from './../../src/types/userTypes';

function generateUsers(count: number) {
  const games: Partial<IUserDetails>[] = [];
  while (--count) {
    games.push({
      is_active: true,
      local: {
        email: Faker.internet.email(),
        password: 'password'
      },
      first_name: Faker.name.firstName(),
      last_name: Faker.name.lastName(),
      zip_code: Faker.address.zipCode(),
      username: Faker.random.word(),
      role: 'user'
    });
  }
  return games;
}

export const trevor = {
  _id: '593a2c245ee35676febbbf9e',
  is_active: true,
  local: {
    email: 'trevthewebdev@gmail.com',
    password: 'password'
  },
  first_name: 'Trevor',
  last_name: 'Pierce',
  phone: '817-555-5555',
  permissions: 5,
  username: 'trevthewebdev',
  role: 'superadmin',
  zip_code: '75071',
  tz: 'America/Chicago'
};

export const mark = {
  _id: '593a2c3f5ee35676febbbfa0',
  is_active: true,
  local: {
    email: 'mark@gmail.com',
    password: 'password'
  },
  first_name: 'Mark',
  last_name: 'Hagood',
  username: 'haygud',
  role: 'user',
  zip_code: '75226',
  tz: 'America/Chicago'
};

export const shannyn = {
  _id: '593a2c385ee35676febbbf9f',
  is_active: true,
  local: {
    email: 'shann@gmail.com',
    password: 'password'
  },
  first_name: 'Shannon',
  last_name: 'Pierce',
  username: 'shanarchy',
  role: 'user',
  zip_code: '75070',
  tz: 'America/Chicago'
};

export const luke = {
  _id: '593a2c465ee35676febbbfa1',
  is_active: true,
  local: {
    email: 'luke@gmail.com',
    password: 'password'
  },
  first_name: 'Luke',
  last_name: 'Skywalker',
  username: 'skywalker',
  role: 'admin',
  zip_code: '75071',
  tz: 'America/Chicago'
};

export const david = {
  _id: '5abfa484a70a874a70b3a144',
  is_active: true,
  local: {
    email: 'dcunkefer@gmail.com',
    password: 'password'
  },
  first_name: 'David',
  last_name: 'Unkefer',
  username: 'dunk',
  role: 'contributor',
  zip_code: '75226',
  tz: 'America/Chicago'
};

export const staticUsers = [trevor, shannyn, mark, luke, david ];
export default [
  ...staticUsers,
  ...generateUsers(8)
];
