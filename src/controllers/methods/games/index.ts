import { slugify } from '../../../utils/requestUtils';
import Games from '../../../models/gameModel';

import logger from '../../../utils/logUtils';
const log = logger.child({ feature: 'games' });

export function updateParentGame(newGame: any) {
  // log.debug('::updateParentGame:: ', newGame);
  return new Promise((resolve, reject) => {
    if (newGame.is_expansion) {
      Games
        .update(
        { _id: newGame.parent_game },
        { $push: { expansions: newGame._id } }
        )
        .then(() => resolve(newGame))
        .catch((err: any) => reject(err));
    } else {
      return resolve(newGame);
    }
  });
}

const editionDictionary: any = {
  1: 'first',
  2: 'second',
  3: 'third',
  4: 'fourth',
  5: 'fifth',
  6: 'sixth',
  7: 'seventh',
  8: 'eighth',
  9: 'nineth',
  10: 'tenth',
};

export function generateAndAssignSlug(result: any[]) {
  const payload: any = result[0];
  const parentGame: any = result[1];
  const { name, edition, is_expansion, slug } = payload;
  let editionString;

  if (edition) {
    editionString = `${editionDictionary[edition]} edition`;
  }

  if (!slug) {
    if (!is_expansion) {
      payload.slug = slugify(name, editionString);
    } else {
      payload.slug = slugify(parentGame.name, editionString, name);
    }
  }

  return payload;
}
