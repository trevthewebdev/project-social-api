import * as express from 'express';
import * as Mongoose from 'mongoose';

const Router = express.Router();

import AuthRoutes from './authRoutes';
import EventRoutes from './eventRoutes';
import GameRoutes from './gameRoutes';
import GroupRoutes from './groupRoutes';
import LocationRoutes from './locationRoutes';
import PlayRoutes from './playRoutes';
import UserRoutes from './userRoutes';

Router.use('/', AuthRoutes);
Router.use('/events', EventRoutes);
Router.use('/games', GameRoutes);
Router.use('/groups', GroupRoutes);
Router.use('/locations', LocationRoutes);
Router.use('/plays', PlayRoutes);
Router.use('/users', UserRoutes);

export default Router;
