import { IApiResponse, IGenericResponse } from '../';
import { GameBaseAttributes } from './gamePrimativeTypes';

export interface IResponseSingleGame extends IGenericResponse {
  type: 'games';
  attributes: GameBaseAttributes;
}

/* Game Listing Responses */
export interface IListingMetaData {
  limit: number;
  skip: number;
  total: number;
}

export type IGameListingResponse = IApiResponse<IResponseSingleGame, IListingMetaData>;

// const thing: IResponseSingleGame = {
//   type: 'games',
//   id: '852973589237538',
//   attributes: {
//     name: 'string',
//     is_archived: true,
//     is_expansion: true,
//     player_min: 123,
//     player_max: 123,
//     min_duration: 123,
//     max_duration: 123,
//     slug: 123
//   },
//   relationships: {
//     game: {
//       data: {
//         type: 'games',
//         id: '86723986724869',
//       }
//     }
//   }
// };

// const response: IGameListingResponse = {
//   data: [thing],
//   meta: {
//     limit: 100,
//     skip: 200,
//     total: 300
//   }
// };
