export const GAME_INCLUDES_SCHEMA: any = {
  base: {
    type: 'games',
    defaultAttributes: [
      'name',
      'edition',
      'is_expansion',
      'is_archived',
      'player_min',
      'player_max',
      'min_duration',
      'max_duration',
      'slug',
      'owned_by',
      'parent_game'
    ]
  },
  parent_game: {
    type: 'games',
    defaultAttributes: [
      'name',
      'edition',
      'is_expansion',
      'is_archived',
      'player_min',
      'player_max',
      'min_duration',
      'max_duration',
      'slug'
    ]
  },
  owned_by: {
    type: 'users',
    defaultAttributes: [
      'first_name', 'last_name', 'username'
    ]
  }
};
