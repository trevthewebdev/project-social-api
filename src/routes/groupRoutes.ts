import * as express from 'express';
import { GroupCreateSchema } from '../validators/groupValidators';
import Groups from '../models/groupModel';

import { setFilters } from '../middleware/request';
import { GROUP_FILTER_MAP } from '../controllers/data/groupfilterMap';
import { validateRequest } from '../validators/shared/customValidators';
import authMiddleware from '../middleware/authMiddleware';
import { gauth } from '../config/auth';

import {
  get,
  getOne,
  post,
  update,
  remove
} from './../controllers/groupController';

const Router = express.Router();
const auth = authMiddleware();

Router.use(auth.authenticate());

Router.get(
  '/',
  setFilters(GROUP_FILTER_MAP),
  get
);

Router.get(
  '/:id',
  gauth.permit('group:read', Groups.findSingleGroup.bind(Groups)),
  getOne
);

Router.post(
  '/',
  gauth.permit('group:create'),
  validateRequest(GroupCreateSchema),
  post
);

Router.patch(
  '/:id',
  gauth.permit('group:update', Groups.findSingleGroup.bind(Groups)),
  update
);

Router.delete(
  '/:id',
  gauth.permit('group:delete', Groups.findSingleGroup.bind(Groups)),
  remove
);

export default Router;
