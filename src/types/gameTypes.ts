import { Request, Response } from 'express';
import { Document, Model } from 'mongoose';

export interface ICategorySummary {
  name:             string;
  category_id:      number;
}

export interface IMechanicSummary {
  name:             string;
  mechanic_id:      number;
  is_primary?:       boolean;
}

type DownloadType = 'jpeg' | 'jpg' | 'png';
export interface IDownloadSummary {
  file_name:        string;
  format:           ImageFormatType;
  public_path:      string;
}

type VideoType = 'youtube' | 'vimeo';
export interface IVideoSummary {
  title?:           string;
  type:             VideoType;
  video_id:         string;
}

type ImageFormatType = 'jpeg' | 'jpg' | 'png';
export interface IImageSummary {
  file_name:        string;
  caption?:         string;
  format:           ImageFormatType;
  public_path:      string;
}

export interface IGameExpansion {
  _id: string;
  name: string;
}

export type GameBase = {
  name:       string;
  slug?:      string;
  owned_by:   string[];
};

export type GameDetails = GameBase & {
  bgg_id?:            number;
  categories?:        ICategorySummary[];
  description?:       string;
  edition?:           number;
  expansions?:        IGameExpansion[];
  is_archived:        boolean;
  is_expansion:       boolean;
  mechanics:          IMechanicSummary[];
  max_duration:       number;
  min_duration:       number;
  media?: {
    downloads?:       IDownloadSummary[];
    videos?:          IVideoSummary[];
    images?:          IImageSummary[];
  };
  owned_by:           string[]; // Todo: Check to see if this needs to be more typesafe
  parent_game?: {
    _id:              string;
    name:             string
  };
  player_best:        number;
  player_max:         number;
  player_min:         number;
  primary_category:   ICategorySummary;
  stats:            {
    total_plays:      number;
    recent_games?:    string[]; // Todo: Come back to this when we establish what game logs look like
  };
  theme?:             string;
  weight?:            number;
};

/*
  Interfaces for API methods
*/
export interface IGameListing {
  type:             'games';
  id:               string;
  name:             string;
  edition?:         number;
  is_expansion:     boolean;
  is_archived:      boolean;
  slug:             string;
  owned_by?:        string[];
}

export interface IListingGameBase {
  type:               'games';
  id:                 string;
  attributes: {
    name:             string;
    edition:          string | null;
    is_archived:      boolean;
    is_expansion:     boolean;
    player_min:       number;
    player_max:       number;
    min_duration:     number;
    max_duration:     number;
    slug:             number;
  };
}

export interface IGameDetails extends GameBase, GameDetails {
  created_at?:     Date;
  updated_at?:     Date;
}

export type IGameCreate = GameBase & Partial<GameDetails>;
export type IGameUpdate = Partial<IGameDetails>;

export interface IGameListingResponse {
  data: IGameListing[] | null;
}

/* Mongoose Document & Model Types */
export interface IGameDocument extends Document, IGameDetails {}
export interface IGameModel extends Model<IGameDocument> {
  findWithCount: (req: Request, res: Response) => any;
}
