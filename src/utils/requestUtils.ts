
type SlugifyType = string;
export function slugify(...args: SlugifyType[]): string {
  const params: SlugifyType[] = Array.prototype.slice.call(args);
  const slug = params.join('-');

  return slug.toLowerCase()
    .replace(/\s+/g, '-')         // Replace spaces with -
    .replace(/[^\w\-]+/g, '')     // Remove all non-word chars
    .replace(/\-\-+/g, '-')       // Replace multiple - with single -
    .replace(/^-+/, '')           // Trim - from start of text
    .replace(/-+$/, '');          // Trim - from end of text
}
