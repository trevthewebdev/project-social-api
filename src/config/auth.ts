import rbac from '../lib/auth/rbac';
import globalRoles from '../lib/auth/roles';

// global rbac model
export const gauth = new rbac(globalRoles);
