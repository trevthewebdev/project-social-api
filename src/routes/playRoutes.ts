import * as express from 'express';
import authMiddleware from '../middleware/authMiddleware';
import {
  get,
  getOne,
  post,
  update,
  remove
} from './../controllers/playController';

const Router = express.Router();
const auth = authMiddleware();

Router.use(auth.authenticate());

// All Games
Router.get('/', get);
Router.get('/:id', getOne);
Router.post('/', post);
// Router.patch('/:id', update);
Router.delete('/:id', remove);

export default Router;
