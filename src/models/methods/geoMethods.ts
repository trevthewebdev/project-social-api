import * as math from 'mathjs';

export function findWithInRadius(lat: number, long: number, maxMiles: number) {
  const maxMeters = math.unit(maxMiles, 'mi').to('m').value;
  const query = {
    loc: {
      $near: {
        $geometry: {
          type: 'Point',
          coordinates: [long, lat]
        },
        $maxDistance: maxMeters
      }
    }
  };

  return this.find(query);
}

export function generateRadialQuery(
  lat: number,
  long: number,
  maxMiles: number = 100,
  minMiles: number = 0
) {
  const maxMeters = math.unit(maxMiles, 'mi').to('m').value;
  const minMeters = math.unit(minMiles, 'mi').to('m').value;

  return {
    $near: {
      $geometry: {
        type: 'Point',
        coordinates: [long, lat]
      },
      $maxDistance: maxMeters,
      $minDistance: minMeters
    }
  };
}
