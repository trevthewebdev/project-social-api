## Project Setup
1. Clone the repo `git clone git@gitlab.com:trevthewebdev/bg-api.git`
2. Install project dependencies `npm install` or `yarn`
3. Copy sample.config.yml to config.yml and edit values to match environment
    - At the time of this writing you just need a database to connect to
4. Seed the database `npm run seed:hard` or `yarn seed:hard`

## Available Commands
### Start the server
`npm start`

### Repopulate the database
`npm run seed:hard`

### Lint project files
`npm run lint`

### Typecheck
`npm run typecheck`

### Run intergration tests
`npm run test`
