import { UserDetails } from '../../../src/types/userTypes';

export function authenticateTestUser(Agent: any, userObj: UserDetails): Promise<string> {
  return new Promise((resolve, reject) => {
    Agent
      .post('/api/v1/authenticate')
      .send({
        email: userObj.local.email,
        password: userObj.local.password
      })
      .then((response: any) => resolve(response.body.data.token))
      .catch((error: any) => reject(error));
  });
}
