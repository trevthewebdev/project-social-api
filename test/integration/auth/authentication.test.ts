import * as Chai from 'chai';
import { expect } from 'chai';

// server & utils
import server from '../../../src/server';

// types & validators
import { IUserDetails } from '../../../src/types/userTypes';
import { UserDetailsSchema } from '../../../src/validators/userValidators';

Chai.use(require('chai-http'));

const Agent = Chai.request.agent(server);

describe('Authentication', () => {
  it('should successfully authenticate with valid credentials', (done) => {
    Agent
      .post('/api/v1/authenticate')
      .send({ email: 'trevthewebdev@gmail.com', password: 'password' })
      .then((response) => {
        const { body } = response;

        expect(response.status).to.eql(200);
        expect(body.data).to.have.property('token');
        expect(body.data).to.have.property('id');
        done();
      });
  });

  it('should fail with UNAUTHORIZED authentication with invalid email', (done) => {
    Agent
      .post('/api/v1/authenticate')
      .send({ email: 'invalid@gmail.com', password: 'password' })
      .catch((error) => {
        const { body } = error.response;

        expect(error.status).to.eql(401);
        expect(body).to.have.property('error', 'Unauthorized');
        done();
      });
  });

  it('should fail with UNAUTHORIZED authentication with invalid password', (done) => {
    Agent
      .post('/api/v1/authenticate')
      .send({ email: 'trevthewebdev@gmail.com', password: 'wrongpass' })
      .catch((error) => {
        const { body } = error.response;

        expect(error.status).to.eql(401);
        expect(body).to.have.property('error', 'Unauthorized');
        done();
      });
  });

  it('should fail with BAD REQUEST when missing EMAIL in request body', (done) => {
    Agent
      .post('/api/v1/authenticate')
      .send({ password: 'wrongpass' })
      .catch((error) => {
        const { body } = error.response;

        expect(error.status).to.eql(400);
        expect(body).to.have.property('error', 'Bad Request');
        expect(body.meta[0]).to.have.property('message', '\"email\" is required');
        expect(body.meta[0].context).to.have.property('key', 'email');
        done();
      });
  });

  it('should fail with BAD REQUEST when missing PASSWORD in request body', (done) => {
    Agent
      .post('/api/v1/authenticate')
      .send({ email: 'trevthewebdev@gmail.com' })
      .catch((error) => {
        const { body } = error.response;

        expect(error.status).to.eql(400);
        expect(body).to.have.property('error', 'Bad Request');
        expect(body.meta[0]).to.have.property('message', '\"password\" is required');
        expect(body.meta[0].context).to.have.property('key', 'password');
        done();
      });
  });
});
