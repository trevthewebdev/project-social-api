import * as _ from 'lodash';
import * as passport from 'passport';
import * as passportJWT from 'passport-jwt';

import config from '../config';
import log from '../utils/logUtils';

import UserModel from '../models/userModel';
import { IUserDetails } from './../types/userTypes';

const ExtractJwt = passportJWT.ExtractJwt;
const Strategy = passportJWT.Strategy;

const params = {
  secretOrKey: config.JWT_SECRET,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

export default () => {
  const strategy = new Strategy(params, (payload: { id: string }, done: any) => {
    const query: any = { _id: payload.id };

    return new Promise((resolve, reject) => (
      UserModel
        .findOne(query)
        .exec()
        .then(
          user => (user) ?
            resolve(done(null, _.pick(user, ['_id', 'is_active', 'role']))) :
            resolve(new Error('User not found'))
        )
    ));
  });

  passport.use(strategy);

  return {
    initialize: () => passport.initialize(),
    authenticate: () => passport.authenticate('jwt', { session: false })
  };
};
