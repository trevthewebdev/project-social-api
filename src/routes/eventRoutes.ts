import * as express from 'express';
import {
  get,
  getOne,
  post,
  update,
  remove
} from './../controllers/eventController';
const Router = express.Router();

import { validateRequest } from '../validators/shared/customValidators';
import { EventCreateSchema, EventUpdateSchema } from '../validators/eventValidators';

Router.get('/', get);
Router.get('/:id', getOne);
Router.post('/', validateRequest(EventCreateSchema), post);
Router.patch('/:id', validateRequest(EventUpdateSchema), update);
Router.delete('/:id', remove);

export default Router;
