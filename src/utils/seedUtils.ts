import config from '../config';
import log from '../../src/utils/logUtils';
import { createDoc } from '../../db';

export const seedCollection = (Schema: any, SEED_DATA: any[], resourceName: string, logProp?: string) => (
  () => {
    const promises = SEED_DATA.map((data: any) => {
      if (config.env !== 'test') {
        console.info(`Adding... ${data[logProp]}`);
      }

      return new Promise((resolve, reject) => {
        createDoc(Schema, data)
          .then(savedData => resolve(savedData))
          .catch(error => {
            if (error.code !== 11000) {
              log.warn(`Failed to add resource ${resourceName}`, error);
            }
            resolve();
          });
      });
    });

    return (
      Promise
        .all(promises)
        .then((data) => log.info(`-- ${data.length} ${resourceName} added successfully --`))
    );
  }
);
