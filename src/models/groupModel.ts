import * as Mongoose from 'mongoose';
import { model, Schema } from 'mongoose';
import { Request, Response } from 'express';

import { IGroupDocument, IGroupModel } from '../types/groupTypes';
import { findWithInRadius } from './methods/geoMethods';

const GroupSchema = new Schema({
  name: {
    $type:          String,
    required:       true
  },
  loc: {
    type: {
      $type:        String,
      default:      'Point'
    },
    coordinates:    [Number],
    country: {
      $type:        String,
      default:      'US'
    },
    city:           String,
    state:          String,
    postal_code:    String
  },
  owner: {
    $type:          Schema.Types.ObjectId,
    ref:            'User'
  },
  members: [{
    $type:          Schema.Types.ObjectId,
    ref:            'User'
  }],
  is_public: {
    $type:          Boolean,
    default:        false
  },
  created_at: {
    $type:          Date,
    require:        true,
    default:        Date.now
  },
  updated_at: {
    $type:          Date,
    require:        true,
    default:        Date.now
  }
}, { typeKey: '$type'});

GroupSchema.index({ loc: '2dsphere' });

GroupSchema.statics.findWithInRadius = findWithInRadius;

GroupSchema.statics.findSingleGroup = function findSingleGroup(req: Request, res: Response) {
  return (
    this
      .findById(req.params.id)
      .then((group: IGroupDocument) => ({ group }))
      .catch((error: Error) => error)
  );
};

export default model<IGroupDocument, IGroupModel>('Group', GroupSchema);
