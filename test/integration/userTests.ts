/* tslint:disable no-var-requires */
import * as Chai from 'chai';
import { expect } from 'chai';
import * as nock from 'nock';
import * as moment from 'moment';
import * as uuid from 'uuid/v1';

import server from './../../src/server';
import config from '../../src/config';

import { IUserDetails, IUserDocument } from './../../src/types/userTypes';
import Users from '../../src/models/userModel';
import { user1, newAdminUser } from './fixtures/userFixture';
import { UserDetailsSchema } from './../../src/validators/userValidators';
import { staticUsers } from '../../seed/data/userSeedData';

const chaiHttp = require('chai-http');
Chai.use(chaiHttp);

const Agent = Chai.request.agent(server);

describe('Users', () => {
  const superAdminUser = staticUsers[0];
  const adminUser = staticUsers[3];
  const shannonUser = staticUsers[1];
  let superAdminToken: string;
  let adminToken: string;
  let shannonToken: string;

  before(function(done: (error?: any) => void) {
    this.timeout(6000);

    const authSuperAdmin = new Promise((resolve, reject) => {
      Agent
        .post('/api/v1/authenticate')
        .send({
          email: superAdminUser.local.email,
          password: superAdminUser.local.password
        })
        .then(response => resolve(response.body.data.token))
        .catch(error => reject(error));
    });
    const authAdmin = new Promise((resolve, reject) => {
      Agent
        .post('/api/v1/authenticate')
        .send({
          email: adminUser.local.email,
          password: adminUser.local.password
        })
        .then(response => resolve(response.body.data.token))
        .catch(error => reject(error));
    });
    const authShannon = new Promise((resolve, reject) => {
      Agent
        .post('/api/v1/authenticate')
        .send({
          email: shannonUser.local.email,
          password: shannonUser.local.password
        })
        .then(response => resolve(response.body.data.token))
        .catch(error => reject(error));
    });

    Promise
      .all([authSuperAdmin, authAdmin, authShannon])
      .then((tokens: any[]) => {
        superAdminToken = tokens[0];
        adminToken = tokens[1];
        shannonToken = tokens[2];
        done();
      })
      .catch(error => console.log(error));
  });

  describe('Adding Users: POST', () => {
    it('should ADD a user: POST /api/v1/users', (done) => {
      Agent
        .post(`/api/v1/users`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(user1)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }

          expect(response).to.have.status(201);
          expect(response.body.data).to.have.property('first_name', user1.first_name);
          expect(response.body.data).to.have.property('last_name', user1.last_name);
          expect(response.body.data.local).to.have.property('email', user1.local.email);
          expect(response.body.data).to.have.property('username', user1.username);
          expect(response.body.data.local).to.not.have.property('password');
          done();
        });
    });

    it('should FAIL to ADD a user with a CONFLICT: POST /api/v1/users', (done) => {
      Agent
        .post('/api/v1/users')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(user1)
        .end((err: Error, response: any) => {
          expect(response).to.have.status(409);
          done();
        });
    });

    it('should FAIL to ADD a user with invalid payload: POST /api/v1/users', (done) => {
      const modifiedParent = Object.assign({}, user1);
      delete modifiedParent.first_name;

      Agent
        .post('/api/v1/users')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(modifiedParent)
        .end((err: Error, response: any) => {
          expect(err).to.have.status(400);
          done();
        });
    });
  });

  describe('Getting Users: GET', () => {
    it('should RETURN all users: /api/v1/users', (done) => {
      Agent
        .get(`/api/v1/users`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }

          const listing = response.body.data;
          const validation = UserDetailsSchema.validate(listing[0]);

          expect(response).to.have.status(200);
          expect(listing).to.be.an('array');
          expect(listing.length).to.be.above(0);
          expect(validation.error).to.be.null;
          done();
        });
    });

    it('should RETURN a SINGLE user by ID: /api/v1/users', (done) => {
      Agent
        .get(`/api/v1/users/${user1._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }

          expect(response).to.have.status(200);
          expect(response.body.data.first_name).to.equal(user1.first_name);
          done();
        });
    });
  });

  describe('Updating Users: PATCH', () => {
    it('should update a SINGLE user: /api/v1/users/:id', (done) => {
      Agent
        .patch(`/api/v1/users/${user1._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send({
          first_name: 'Kevin',
          last_name: 'Williams'
        })
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }

          expect(response).to.have.status(204);
          done();
        });
    });

    it('should FAIL to UPDATE a SINGLE user with invalid id param: PATCH /api/v1/user/:id', (done) => {
      Agent
        .patch(`/api/v1/users/123ese`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send({ first_name: 'Kevin' })
        .end((err: Error, response: any) => {
          expect(err).to.have.status(500);
          done();
        });
    });
  });

  describe('Removing Users: DELETE', () => {
    it('should delete a SINGLE user: /api/v1/users', (done) => {
      Agent
        .del(`/api/v1/users/${user1._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .end((err: Error, response: any) => {
          if (err) {
            return done(err);
          }

          expect(response).to.have.status(204);
          done();
        });
    });
  });

  describe('Request Password Reset', () => {
    it('should successfully request a password reset', (done) => {
      nock(`${config.emailService.uri}/api/v1/sendmail`)
        .post('')
        .reply('200', {});

      Agent
        .post('/api/v1/password/reset')
        .send({ email: 'trevthewebdev@gmail.com' })
        .then(() => Users.findOne({ 'local.email': 'trevthewebdev@gmail.com' }))
        .then((userDoc: any) => {
          const expiresCheck = moment(userDoc.password_reset.expires).isBefore(moment().add(20, 'minutes'));

          expect(userDoc.password_reset).to.have.property('token');
          expect(userDoc.password_reset).to.have.property('expires');
          expect(expiresCheck).to.be.true;
          nock.restore();
          done();
        });
    });

    it('should fail to request a password when user does not exist', (done) => {
      Agent
        .post('/api/v1/password/reset')
        .send({ email: 'invalid@gmail.com' })
        .catch((error) => {
          const { body } = error.response;

          expect(error.status).to.eql(404);
          expect(body).to.have.property('error', 'Not Found');
          done();
        });
    });
  });

  describe('User Permissions:', () => {
    describe('User Role', () => {
      it('should succeed when a user tries to update themselves', (done) => {
        Agent
          .patch(`/api/v1/users/${shannonUser._id}`)
          .set('Authorization', `Bearer ${shannonToken}`)
          .send({
            first_name: 'Shannyn'
          })
          .then((response: any) => {
            expect(response).to.have.status(204);
            done();
          });
      });

      it('should fail with 403 when a user tries to update another user', (done) => {
        Agent
          .patch(`/api/v1/users/${staticUsers[2]}`)
          .set('Authorization', `Bearer ${shannonToken}`)
          .send({
            first_name: 'Shannyn'
          })
          .catch((response: any) => {
            expect(response).to.have.status(403);
            done();
          });
      });

      it('should fail with 403 when a user tries to create an admin', (done) => {
        Agent
          .post('/api/v1/users')
          .set('Authorization', `Bearer ${shannonToken}`)
          .send(newAdminUser)
          .catch((response: any) => {
            expect(response).to.have.status(403);
            done();
          });
      });
    });

    describe('Admin Role', () => {
      it('should succeed when an admin tries to update another user', (done) => {
        Agent
          .patch(`/api/v1/users/${shannonUser._id}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .send({
            first_name: 'Shannunz'
          })
          .then((response: any) => {
            expect(response).to.have.status(204);
            done();
          });
      });

      it('should fail with 403 when an admin tries to create an admin', (done) => {
        Agent
          .post('/api/v1/users')
          .set('Authorization', `Bearer ${adminToken}`)
          .send(newAdminUser)
          .catch((response: any) => {
            expect(response).to.have.status(403);
            done();
          });
      });
    });

    describe('Super Admin Role', () => {
      it('should succeed when a user tries to create an admin', (done) => {
        Agent
          .post('/api/v1/users')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .send(newAdminUser)
          .then((response: any) => {
            expect(response).to.have.status(201);
            done();
          });
      });
    });
  });

  describe('Confirm Password Reset', () => {
    const dummyUsers = [
      {
        local: {
          email: 'dummy1@gmail.com',
          password: 'password'
        },
        first_name: 'Dummy',
        last_name: 'Smith',
        password_reset: {
          token: uuid(),
          expires: moment().add(15, 'minutes').toISOString()
        },
        username: 'dummy1',
        zip_code: '75021'
      },
      {
        local: {
          email: 'dummy2@gmail.com',
          password: 'password'
        },
        first_name: 'Dummy',
        last_name: 'Jones',
        password_reset: {
          token: uuid(),
          expires: moment().subtract(15, 'minutes').toISOString()
        },
        username: 'dummy2',
        zip_code: '75021'
      }
    ];

    before((done: (error?: any) => void) => {
      Users
        .insertMany(dummyUsers)
        .then((something: any) => done());
    });

    it('should successfully reset password', (done) => {
      Agent
        .post('/api/v1/password/confirm')
        .send({
          password: 'newPassword',
          token: dummyUsers[0].password_reset.token
        })
        .then(response => {
          expect(response).to.have.status(204);

          return Users.findOne({ 'local.email': 'dummy1@gmail.com' });
        })
        .then((userDoc: any) => {
          expect(userDoc.password_reset).to.have.property('token', '');
          expect(userDoc.password_reset).to.have.property('expires', null);

          return userDoc.comparePassword(dummyUsers[0].local.password);
        })
        .then((isSamePassword: any) => {
          expect(isSamePassword).to.be.false;
          done();
        });
    });

    it('should fail with 400 if sent an invalid token', (done) => {
      Agent
        .post('/api/v1/password/confirm')
        .send({
          password: 'newPassword',
          token: '123'
        })
        .catch(error => {
          const { body } = error.response;

          expect(error).to.have.status(400);
          expect(body.meta[0]).to.have.property('message', '\"token\" must be a valid GUID');
          expect(body.meta[0].context).to.have.property('key', 'token');
          done();
        });
    });

    it('should fail with 404 if sent wrong token', (done) => {
      Agent
        .post('/api/v1/password/confirm')
        .send({
          password: 'newPassword',
          token: uuid()
        })
        .catch(error => {
          const { body } = error.response;

          expect(error).to.have.status(404);
          expect(body).to.have.property('error', 'Not Found');
          done();
        });
    });

    it('should fail with 400 if reset time has expired', (done) => {
      Agent
        .post('/api/v1/password/confirm')
        .send({
          password: 'newPassword',
          token: dummyUsers[1].password_reset.token
        })
        .catch(error => {
          const { body } = error.response;

          expect(error).to.have.status(400);
          expect(body).to.have.property('error', 'Bad Request');
          expect(body.errors[0].description)
            .to.equal(`invalid token or expire time: ${dummyUsers[1].password_reset.token}`);
          expect(body.errors[0].meta).to.have.property('token', dummyUsers[1].password_reset.token);
          done();
        });
    });

    it('should fail with 400 if not sent a password to replace old password', (done) => {
      Users
        .findOne({ 'local.email': 'trevthewebdev@gmail.com' })
        .then((userDoc: IUserDocument) => {
          return new Promise((resolve, reject) => {
            Agent
              .post('/api/v1/password/confirm')
              .send({
                token: userDoc.password_reset.token
              })
              .catch(error => reject(error));
          });
        })
        .catch(error => {
          const { body } = error.response;

          expect(error).to.have.status(400);
          expect(body.meta[0]).to.have.property('message', '\"password\" is required');
          expect(body.meta[0].context).to.have.property('key', 'password');
          done();
        });
    });

    xit('should fail password strength validation', (done) => {
      expect(true).to.be.false;
      done();
    });
  });
});
