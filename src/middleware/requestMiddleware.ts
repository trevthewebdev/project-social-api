import { Request, Response, NextFunction } from 'express';

import log from '../utils/logUtils';

export function idOrSlug(req: Request, res: Response, next: NextFunction) {
  if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
    req.app.locals.findBy = '_id';
  } else {
    req.app.locals.findBy = 'slug';
  }
  next();
}
