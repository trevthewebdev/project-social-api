import { Request, Response } from 'express';
import { Document, Model } from 'mongoose';

import { IUserDetails } from './userTypes';
import { IGameDetails } from './gameTypes';
import {
  Attendee,
  EventLocation,
  EventStatus,
  EventSummary
} from './eventTypes';

export type EventStatus = 'confirmed' | 'tenative' | 'cancelled';

/* Describes the details of an event */
export interface IEventDetails {
  from: number;
  to: number;
  tz: string;
  attendees: Attendee[];
  group?: string;
  organizer: string;
  loc: EventLocation;
  public: boolean;
  repeats: boolean;
  status: EventStatus;
  created_at: Date;
  update_at?: Date;
}
export type IEventDetailsCreate = {
  from: number;
  to: number;
  tz: string;
  attendees: string[] & Attendee[];
  group?: string;
  organizer: string;
  loc: EventLocation;
  public?: boolean;
  repeats?: boolean;
  status: EventStatus;
};

export type IEventDetailsUpdate = Partial<IEventDetails>;

/* Mongoose Document & Model Types */
export interface IEventDetailsDocument extends Document, IEventDetails {}
export interface IEventDetailsModel extends Model<IEventDetailsDocument> {}
