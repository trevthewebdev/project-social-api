import * as Joi from 'joi';

import config from '../config';
import { Validator } from '../types/genericTypes';
import {
  GroupBase,
  GroupCreate,
  IGroupDetails,
  GroupUpdate
} from '../types/groupTypes';

const groupBaseSchemaMap: Validator<GroupBase> = {
  members: Joi.array().items(
    Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  ).min(1),
  name: Joi.string().max(50, 'utf8').required(),
  owner: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
  is_public: Joi.boolean(),
};

export const GroupBaseSchema = Joi.object(groupBaseSchemaMap);

// Create
const groupCreateSchemaMap: Validator<GroupCreate> = Object.assign({}, groupBaseSchemaMap, {
  is_public: Joi.boolean(),
  loc: Joi.object({
    coordinates: Joi.array().items(Joi.number()).required(),
    country: Joi.string().min(2).max(2),
    postal_code: Joi.string().min(5).max(9),
    state: Joi.string().min(2).max(2).required()
  }).required(),
  members: Joi.array().items(Joi.string()).min(1).required()
});

let GroupCreateSchemaTemp = Joi.object(groupCreateSchemaMap);
if (config.env === 'test') {
  GroupCreateSchemaTemp = GroupCreateSchemaTemp.keys({
    _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  });
}

export const GroupCreateSchema = GroupCreateSchemaTemp;

// Update
const groupUpdateSchemaMap: Validator<GroupUpdate> = Object.assign({}, groupBaseSchemaMap, {
  owner: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  name: Joi.string().max(50, 'utf8')
});

export const GroupUpdateSchema = GroupBaseSchema.keys(groupUpdateSchemaMap);

// Details
const groupDetailsSchemaMap: Validator<IGroupDetails> = Object.assign({}, groupBaseSchemaMap, {
  _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  avatar: Joi.string().allow(null).uri({
    scheme: ['http', 'https']
  }).example('https://www.google.com'),
  loc: Joi.object({
    coordinates: Joi.array().items(Joi.number()),
    country: Joi.string().min(2).max(2),
    postal_code: Joi.string().min(5).max(9),
    state: Joi.string().min(2).max(2)
  }),
  members: Joi.array().items(
    Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  ).min(1),
  name: Joi.string().max(50, 'utf8'),
  is_public: Joi.boolean(),
  owner: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  created_at: Joi.date().iso(),
  updated_at: Joi.date().iso()
});

export const GroupDetailsSchema = GroupBaseSchema.keys(groupDetailsSchemaMap);
