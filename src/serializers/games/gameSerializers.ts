import { isEmpty } from 'lodash';
import { Serializer } from 'jsonapi-serializer';

type Fields = { [key: string]: string[] };

export function generateSerializer(SCHEMA: any, type: string, includedKeys: any[] = [], fields: Fields) {
  let includes = {};
  if (includedKeys.length) {
    includes = includedKeys.reduce((acc, keyName) => {
      const schemeConfig = SCHEMA[keyName];
      const includedFields: string[] = fields[schemeConfig.type];

      acc[keyName] = {
        ref: '_id',
        attributes: includedFields || schemeConfig.defaultAttributes,
        included: true
      };

      return acc;
    }, {});
  }

  const gameFields = isEmpty(fields.games) ? [] : fields.games;
  return new Serializer(type, {
    keyForAttribute: 'underscore_case',
    typeForAttribute: (attr: string) => {
      return attr === 'games' ? 'games' : SCHEMA[attr].type;
    },
    attributes: [...SCHEMA.base.defaultAttributes, ...includedKeys, ...gameFields],
    ...includes
  });
}
