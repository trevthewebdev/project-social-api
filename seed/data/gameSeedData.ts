import * as _ from 'lodash';
import * as Faker from 'faker';

import config from '../../src/config';
import { IGameDetails } from './../../src/types/gameTypes';
import { slugify } from '../../src/utils/requestUtils';

function generateGames(count: number) {
  const games: Partial<IGameDetails>[] = [];
  while (--count) {
    const name: string = Faker.random.words();
    const minDuration: number = _.random(10, 60);

    games.push({
      name,
      slug: slugify(name),
      player_min: _.random(2, 5),
      player_max: _.random(2, 5),
      min_duration: minDuration,
      max_duration: _.random(minDuration + 10, 240),
      weight: _.random(1, 5)
    });
  }
  return games;
}

export const staticGames = [
  {
    _id: '5939dbfa5ee35676febbbf9b',
    name: 'Zombicide Black Plague',
    slug: 'zombicide-black-plague',
    player_min: 1,
    player_max: 6,
    player_best: 3,
    min_duration: 90,
    max_duration: 240,
    mechanics: [
      'dice_rolling',
      'modular_board',
      'variable_player_powers'
    ],
    weight: 2,
    owned_by: [
      '593a2c245ee35676febbbf9e'
    ],
    primary_mechanic: 'dice_rolling',
    expansions: [
      '5939de465ee35676febbbf9c'
    ],
    player_interaction: ['coop']
  },
  {
    _id: '5939de465ee35676febbbf9c',
    name: 'Wulfsburg',
    slug: 'zombicide-black-plague-wulfsburg',
    is_expansion: true,
    player_min: 1,
    player_max: 6,
    player_best: 3,
    parent_game: '5939dbfa5ee35676febbbf9b',
    min_duration: 90,
    max_duration: 240,
    weight: 2,
    owned_by: [
      '593a2c245ee35676febbbf9e'
    ],
    player_interaction: ['coop']
  },
  {
    _id: '5939de4d5ee35676febbbf9d',
    name: 'Raiders of the North Sea',
    slug: 'raiders-of-the-north-sea',
    player_min: 2,
    player_max: 4,
    player_best: 3,
    min_duration: 45,
    max_duration: 120,
    weight: 3,
    owned_by: [
      '593a2c465ee35676febbbfa1'
    ],
    player_interaction: ['competitive']
  },
  {
    _id: '593783dc5ee35676febbbf98',
    name: 'Lords of Waterdeep',
    slug: 'lords-of-waterdeep',
    player_min: 2,
    player_max: 5,
    player_best: 3,
    min_duration: 60,
    max_duration: 120,
    owned_by: [
      '593a2c245ee35676febbbf9e'
    ],
    expansions: [
      '593783e95ee35676febbbf99'
    ],
    player_interaction: ['competitive']
  },
  {
    _id: '593783e95ee35676febbbf99',
    name: 'Scoundrels of Skullport',
    slug: 'scoundrels-of-skullport',
    is_expansion: true,
    parent_game: '593783dc5ee35676febbbf98',
    player_min: 2,
    player_max: 6,
    player_best: 4,
    min_duration: 60,
    max_duration: 60,
    owned_by: [
      '593a2c245ee35676febbbf9e'
    ],
    player_interaction: ['competitive']
  },
  {
    _id: '593784f55ee35676febbbf9a',
    name: 'Evolution',
    slug: 'evolution',
    player_min: 2,
    player_max: 6,
    player_best: 4,
    is_archived: true,
    min_duration: 50,
    max_duration: 70,
    mechanics: [
      'hand_management',
      'simultaneous_action_selection',
      'take_that'
    ],
    owned_by: [
      '593a2c245ee35676febbbf9e',
      '593a2c465ee35676febbbfa1'
    ],
    primary_mechanic: 'hand_management',
    player_interaction: ['competitive']
  }
];

export default [
  ...staticGames,
  ...generateGames(config.env === 'test' ? 5 : 70)
];
