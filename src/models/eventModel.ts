import * as _ from 'lodash';
import { model, Schema, Document } from 'mongoose';

import { IEventDocument, IEventModel, IEventCreate } from '../types/eventTypes';
import { findWithInRadius } from './methods/geoMethods';

import EventDetails from './eventDetailsModel';
import { IEventDetails } from 'eventDetailsTypes';

const EventSchema = new Schema({
  from: {
    $type: Number,
    required: true
  },
  to: {
    $type: Number,
    required: true
  },
  tz: {
    $type: String,
    required: true
  },
  attendees: [{
    status: {
      $type: String,
      default: 'accepted'
    },
    role: {
      $type: String,
      default: 'opt-participant'
    },
    user: {
      $type: Schema.Types.ObjectId,
      ref: 'User'
    }
  }],
  group: {
    $type: Schema.Types.ObjectId,
    ref: 'Group'
  },
  loc: {
    type: {
      $type: String,
      default: 'Point'
    },
    coordinates: [Number],
    country: {
      $type: String,
      default: 'US',
      max: 2,
      min: 2,
      required: true
    },
    state: {
      $type: String,
      required: true,
      max: 2,
      min: 2
    },
    address_1: String,
    address_2: String,
    city: String,
    postal_code: String
  },
  organizer: {
    $type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  public: {
    $type: Boolean,
    default: false
  },
  repeats: {
    $type: Boolean,
    default: false
  },
  status: {
    $type: String,
    required: true
  },
  created_at: {
    $type: Date,
    require: true,
    default: Date.now
  },
  updated_at: {
    $type: Date,
    require: true,
    default: Date.now
  }
}, { typeKey: '$type' });

EventSchema.index({ loc: '2dsphere' });

EventSchema.statics.findWithInRadius = findWithInRadius;
EventSchema.statics.createWithDetails = function createWithDetails(payload: IEventCreate) {
  return (
    this
      .create(payload)
      .then((newEvent: Document) => (
        EventDetails
          .create({
            ..._.pick(newEvent, [
              'attendees', 'from', 'to', 'tz', 'status', 'public'
            ]),
            parent_event: newEvent._id
          })
          .then((newEventDetails) => ({
            type: 'events',
            attributes: newEvent,
            included: [{
              type: 'eventdetails',
              id: newEventDetails._id,
              attrubutes: _.omit(newEventDetails, '_id', '__v')
            }]
          }))
      ))
  );
};

export default model<IEventDocument, IEventModel>('Event', EventSchema);
