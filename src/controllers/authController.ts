import * as uuid from 'uuid/v1';
import * as Joi from 'joi';
import * as jwt from 'jwt-simple';
import * as moment from 'moment';
import { Request, Response } from 'express';

import config from '../config';
import logger from '../utils/logUtils';
import MetaError from '../utils/errorUtils';
import { catchAndRespond } from './methods/global';
import { NotificationService } from '../config/axios';

import Users from '../models/userModel';
import { IUserDocument } from './../types/userTypes';

const log = logger.child({ feature: 'auth' });

/* Move this */
export interface IAuth {
  email: string;
  password: string;
}

export interface IConfirmResetPayload {
  password: string;
  token: string;
}

type AuthSchemaMap = {
  [K in keyof IAuth]: Joi.Schema;
};

type ConfirmSchemaMap = {
  [K in keyof IConfirmResetPayload]: Joi.Schema;
};

const authPayloadSchemaMap: AuthSchemaMap = {
  email: Joi.string().email().required(),
  password: Joi.string().required()
};

const confirmResetPayloadSchemaMap: ConfirmSchemaMap = {
  password: Joi.string().required(),
  token: Joi.string().guid().required()
};

const AuthPayloadSchema = Joi.object(authPayloadSchemaMap);
const ConfirmResetPayloadSchema = Joi.object(confirmResetPayloadSchemaMap);
/* End Move this */

export function authenticate(req: Request, res: Response) {
  const { email, password } = req.body;
  const { error } = AuthPayloadSchema.validate(req.body);

  if (error) {
    return res.boom.badRequest(error.message, { meta: error.details });
  }

  Users
    .findOne({ 'local.email': email })
    .exec()
    .then((user: IUserDocument) => {
      if (!user) {
        return Promise.reject(new Error('401'));
      }

      return user.comparePassword(password)
        .then((result) => ({
          user,
          match: result
        }));
    })
    .then((result: any) => {
      if (!result.match) {
        return Promise.reject(new Error('401'));
      }

      const token = jwt.encode({
        id: result.user._id,
        iat: moment().unix(),
        exp: moment().add(process.env.JWT_EXPIRE_TIME, 'hours').unix()
      }, config.JWT_SECRET);

      res.status(200).json({
        data: {
          token,
          id: result.user._id,
          role: result.user.role
        }
      });
      return;
    })
    .catch((err: any) => {
      switch (err.message) {
        case '401':
          res.boom.unauthorized();
          break;
        default:
          res.boom.badImplementation();
      }
    });
}

export function requestResetPassword(req: Request, res: Response) {
  const { email } = req.body;
  Users
    .findOne({ 'local.email': email })
    .then((user: IUserDocument) => {
      if (!user) {
        return Promise.reject(
          new MetaError('Not found', {
            type: 'not found'
          })
        );
      }

      return Promise.resolve(user);
    })
    .then((user: IUserDocument) => user.setPasswordReset())
    .then((user) => (
      NotificationService
        .post('/api/v1/sendmail', {
          email: user.email,
          email_type: 'password-forgot',
          first_name: user.first_name,
          token: user.token
        })
    ))
    .then(() => res.status(202).json())
    .catch(catchAndRespond(req, res, log));
}

export function resetPassword(req: Request, res: Response) {
  const { password, token } = req.body;
  const { error } = ConfirmResetPayloadSchema.validate(req.body);

  if (error) {
    return res.boom.badRequest(error.message, { meta: error.details });
  }

  Users
    .findOne({ 'password_reset.token': token })
    .then((user: IUserDocument) => {
      if (!user) {
        return Promise.reject(
          new MetaError('Not found', {
            type: 'not found'
          })
        );
      }

      return user.checkResetToken(token);
    })
    .then((user: any) => {
      user.local.password = password;
      user.password_reset.expires = null;
      user.password_reset.token = '';
      return user.save();
    })
    .then(() => res.status(204).json())
    .catch(catchAndRespond(req, res, log));
}
