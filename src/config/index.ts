import * as fs from 'fs';
import * as path from 'path';
import * as Bluebird from 'bluebird';

const SECRETS_DIR = '/run/secrets';
const { JWT_EXPIRE_TIME, NODE_ENV, SERVICE_PORT } = process.env;

let secrets: any = {
  JWT_EXPIRE_TIME,
  NODE_ENV: NODE_ENV || 'development',
  SERVICE_PORT,
};

if (fs.existsSync(SECRETS_DIR)) {
  const files = fs.readdirSync(SECRETS_DIR);

  secrets = files.reduce((accum: any, fileName: string) => {
    const fullpath = path.join(SECRETS_DIR, fileName);
    const key = fileName.toUpperCase();
    const data = fs.readFileSync(fullpath, 'utf8').toString().trim();

    accum[key] = data;
    return accum;
  }, { ...secrets });
}

global.Promise = Bluebird;

export default secrets;
