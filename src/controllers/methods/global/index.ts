import { Request, Response } from 'express';
import MetaError from '../../../utils/errorUtils';
import * as _ from 'lodash';

/**
 * Generic: Query All
 * @description generic / general purpose handler for querying resources
 * @param Model mongoose schema
 * @param query mongoose query object
 * @param options mongoose query options
 */
const queryListInitialOptions = { limit: 20, skip: 0, sort: {}, __v: 0 };

export function queryList(
  Model: any,
  query: any = {},
  queryOptions: any = {}
) {
  const options = Object.assign({}, queryListInitialOptions, queryOptions);
  const { sort, limit, skip, where, ...rest } = options;
  let dbQuery: any = query || {};

  if (!_.isEmpty(where)) {
    dbQuery = Object.assign({}, dbQuery, where);
  }

  return (
    Promise.all([
      Model
        .find(dbQuery, { ...rest })
        .limit(parseInt(limit, 0))
        .skip(parseInt(skip, 0))
        .sort(sort),
      Model
        .find(dbQuery)
        .count()
    ])
    .then((results: any) => Promise.resolve([
      results[0],
      {
        limit,
        skip,
        total: results[1]
      }
    ]))
    .catch((error: Error) => Promise.reject(error))
  );
}

/**
 * Generic: Find One Resource
 * @description atomic function to return a game
 * @param Model mongoose schema
 * @param query mongoose query object
 */
export function findOneResource(
  Model: any,
  query: any,
  queryOptions: any = {}
) {
  const options = Object.assign({}, { __v: 0 }, queryOptions);
  return Model.findOne(query, options);
}

export function checkAlreadyExists(
  payload: any,
  log?: any
) {
  return (existingDocument: any) => {
    if (existingDocument) {
      return Promise.reject(
        new MetaError('Conflict: duplicate found', {
          type: 'conflict',
          existingDocument: existingDocument._id
        })
      );
    }
    return Promise.resolve(payload);
  };
}

/**
 * Generic: Save Document
 * @param Model mongoose schema
 * @param payload incoming from request
 */
export function saveDocument(Model: any, log: any) {
  return (payload: any) => {
    return (
      Model.create(payload)
        .then((result: any) => result)
        .catch((err: any) => {
          let newError;
          const error = (typeof err.toJSON === 'function')
            ? err.toJSON()
            : err;

          if (error.code === 11000) {
            newError = new MetaError('Conflict: mongo conflict', {
              type: 'conflict',
              existingDocument: error.op._id
            });
          } else {
            newError = MetaError('Error createing resource', error);
          }

          return Promise.reject(newError);
        })
    );
  };
}

/**
 * Generic: Catch Statement
 * @description generic catch statement for all controllers
 */
export function catchAndRespond(
  req: Request,
  res: Response,
  log: any) {
  return (error: any) => {
    console.log(error);
    const type: string = (error.meta) ? error.meta.type : '';
    log.error('::catchAndRespond::', error.message);
    log.debug(error.stack);

    switch (type) {
      case 'conflict':
        res.boom.conflict(null, {
          errors: [{
            title: 'conflict',
            meta: {
              existingDocument: error.meta.existingDocument
            }
          }]
        });
        break;

      case 'not found':
        res.boom.notFound(null, {
          errors: [{
            title: 'not found'
          }]
        });
        break;

      case 'forbidden':
        res.boom.forbidden(null, {
          errors: [{
            title: 'forbidden'
          }]
        });
        break;

      case 'bad request':
        res.boom.badRequest(null, {
          errors: [{
            title: 'bad request',
            description: error.message,
            meta: {
              token: error.meta.token
            }
          }]
        });
        break;

      default:
        res.boom.badImplementation(error);
    }
  };
}
