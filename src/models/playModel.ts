import * as Mongoose from 'mongoose';
import { Schema } from 'mongoose';
import { required } from 'joi';
// import { IUserDetails } from './../types/userTypes';

export type IPlayModel = {} & Mongoose.Document;

const options = { discriminatorKey: 'kind' };

const PlaySchema = new Mongoose.Schema({
  game: {
    type: Schema.Types.ObjectId,
    ref: 'Game',
    required: true
  },
  players: [{
    player: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    score: Number
  }],
  duration: {
    type: Number,
    required: true
  },
  date_played: {
    type: Date,
    required: false
  },
  high_score: {
    type: Number
  },
  high_score_player: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
}, options);

const PlayModel = Mongoose.model<IPlayModel>('Play', PlaySchema);

const CompetitivePlaySchema = PlayModel.discriminator(
  'competitive',
  new Mongoose.Schema({
    winners: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  }, options)
);

const CoopPlaySchema = PlayModel.discriminator(
  'coop',
  new Mongoose.Schema({
    win: Boolean
  }, options)
);

const SimiCoopPlaySchema = PlayModel.discriminator(
  'simi-coop',
  new Mongoose.Schema({
    winners: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }]
  }, options)
);

export { CoopPlaySchema as CoopPlay };
export { SimiCoopPlaySchema as SimiCoopPlay };
export { CompetitivePlaySchema as CompetitivePlay };

export default PlayModel;
