import * as Mongoose from 'mongoose';
import * as Bluebird from 'bluebird';
import { Model, Document } from 'mongoose';

import config from './src/config';

export function connectToDb() {
  let options: any = { useNewUrlParser: true };
  let mongoURI = `mongodb://mongodb:${config.MONGO_PORT}/${config.MONGO_DB_NAME}`;

  if (typeof config.MONGO_URL !== 'undefined') {
    mongoURI = `mongodb+srv://${config.MONGO_USER}:${config.MONGO_PASS}@${config.MONGO_URL}/${config.MONGO_DB_NAME}`
    options = {
      ...options,
      authSource: 'admin',
      replicaSet: `${config.MONGO_SET_NAME}-shard-0`,
      retryWrites: true,
      ssl: true
    };
  }

  (Mongoose as any).Promise = Bluebird;
  Mongoose
    .connect(mongoURI, options)
    .then(() => console.log(`Connected to MongoDb`))
    .catch((error: Error) => {
      if (error.message && error.message.search('MongoError: connect ECONNREFUSED') !== -1) {
        console.error(`Check to make sure you've started the mongo service`, error.message);
      } else {
        console.error(error);
      }
    });
}

// Generic create document
export function createDoc(model: Model<Document>, doc: any ) {
  return new Promise((resolve, reject) => {
    new model(doc).save((err: Error, saved: any) => {
      return err ? reject(err) : resolve(saved);
    });
  });
}

export function cleanDB(models: any) {
  const cleanPromises = models.map((model: any) => {
    return model.remove().exec();
  });

  return Promise.all(cleanPromises);
}
