import * as path from 'path';
import * as bunyan from 'bunyan';
import * as moment from 'moment';
import * as _ from 'lodash';

import { Stream } from 'bunyan';
import config from '../config';

const isProduction = config.env === 'production';

let mainLoggerStreams: Stream[] = [{
  stream: process.stdout
}];

if (isProduction) {
  mainLoggerStreams = [{
    path: path.resolve(config.LOG_PATH, 'service.log')
  }];
}

// main logger
const logger = bunyan.createLogger({
  name: 'overture api',
  level: config.LOG_LEVEL,
  serializers: bunyan.stdSerializers,
  streams: mainLoggerStreams,
  src: isProduction ? false : true
});

// request logger
export function requestLogger(req: any, res: any, next: any) {
  const oldEnd = res.end;
  const reqChildConfig = {
    req_id: req.req_id.split('-')[0]
  };

  req.log = logger.child({ ...reqChildConfig, tag: 'req' });
  res.log = logger.child({ ...reqChildConfig, tag: 'res' });

  res.log.info({ req }, '< %s: %s',
    req.method,
    req.originalUrl
  );

  res.end = () => {
    let level = 'info';

    if (res.statusCode >= 400) {
      level = 'warn';
    }

    if (res.statusCode >= 500) {
      level = 'error';
    }

    res.log[level]({ res }, '> %s: %s %d',
      req.method,
      req.originalUrl,
      res.statusCode,
    );

    // extends the old end method
    oldEnd.apply(res, arguments);
  };

  next(null);
}

export default logger;
