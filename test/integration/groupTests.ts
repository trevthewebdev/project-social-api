/* tslint:disable no-var-requires */
import * as Chai from 'chai';
import { expect } from 'chai';
import * as _ from 'lodash';
const chaiHttp = require('chai-http');

import { authenticateTestUser } from './utils/authTestUtils';
import server from './../../src/server';
import {
  GroupDetailsSchema
} from './../../src/validators/groupValidators';
import {
  staticGroups,
  testGroup1,
  testGroup2
} from '../../seed/data/groupSeedData';
import { staticUsers } from '../../seed/data/userSeedData';

Chai.use(chaiHttp);

const Agent = Chai.request.agent(server);

describe('Groups', () => {
  const superAdminUser = staticUsers[0];
  const amdinUser = staticUsers[3];
  const contributorUser = staticUsers[4];
  const user1 = staticUsers[1];
  let superAdminToken: string;
  let adminToken: string;
  let contributorToken: string;
  let user1Token: string;

  before(function(done: (error?: any) => void) {
    this.timeout(6000);

    Promise
      .all([
        authenticateTestUser(Agent, superAdminUser),
        authenticateTestUser(Agent, amdinUser),
        authenticateTestUser(Agent, contributorUser),
        authenticateTestUser(Agent, user1),
      ])
      .spread((superAdmin: string, admin: string, contributor: string, userOne: string) => {
        superAdminToken = superAdmin;
        adminToken = admin;
        user1Token = userOne;
        contributorToken = contributor;
        done();
      })
      .catch(error => console.log(error));
  });

  describe('Adding Groups: POST /api/v1/groups', () => {
    it('should successfully add a group', (done) => {
      Agent
        .post('/api/v1/groups')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(testGroup1)
        .then((response: any) => {
          const newGroup = response.body.data;

          expect(response).to.have.status(201);
          expect(newGroup).to.have.property('name', testGroup1.name);
          expect(newGroup).to.have.property('is_public', testGroup1.is_public);
          expect(newGroup).to.have.property('owner', testGroup1.owner);
          expect(newGroup.members).to.be.an('array');
          expect(newGroup.members.length).to.be.above(0);
          done();
        });
    });

    it('should add a group with returned game adding the new owner as a member', (done) => {
      Agent
        .post('/api/v1/groups')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send({
          _id: '5acd8c8842d8074adf72dca1',
          name: 'Random Group',
          is_public: false,
          owner: '593a2c3f5ee35676febbbfa0',
          loc: {
            coordinates: [-96.800258, 32.779071],
            state: 'TX'
          },
          members: ['593a2c465ee35676febbbfa1']
        })
        .then((response: any) => {
          const newGroup = response.body.data;

          expect(response).to.have.status(201);
          expect(newGroup.members).to.be.an('array');
          expect(newGroup.members.length).to.be.above(0);
          expect(newGroup.members[0]).to.equal(newGroup.owner);
          done();
        });
    });

    it('should fail to add a group with a name & state conflict', (done) => {
      Agent
        .post('/api/v1/groups')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(testGroup1)
        .catch((error: Error) => {
          expect(error).to.have.status(409);
          done();
        });
    });

    it('should fail to add a group with invalid payload', (done) => {
      Agent
        .post('/api/v1/groups')
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send({
          name: 'Invalid Payload'
        })
        .catch((error: Error) => {
          expect(error).to.have.status(400);
          done();
        });
    });
  });

  describe('Getting Groups: GET /api/v1/groups', () => {
    it('should successfully return a list of PUBLIC groups within 10 miles', (done) => {
      Agent
        .get('/api/v1/groups?filter[public]=true&filter[lat]=33.039783&filter[long]=-96.732162&filter[max_miles]=10')
        .set('Authorization', `Bearer ${user1Token}`)
        .then((response: any) => {
          const groups = response.body.data;
          const group = _.sample(groups);
          expect(groups).to.be.an('array');
          expect(groups.length).to.be.above(0);
          expect(group).to.have.property('is_public', true);
          expect(response.body.meta.total).to.eql(1);
          done();
        });
    });

    it('should successfully return a list of PUBLIC groups within 30 miles', (done) => {
      Agent
        .get('/api/v1/groups?filter[public]=true&filter[lat]=33.039783&filter[long]=-96.732162&filter[max_miles]=30')
        .set('Authorization', `Bearer ${user1Token}`)
        .then((response: any) => {
          const groups = response.body.data;
          const group = _.sample(groups);
          expect(groups).to.be.an('array');
          expect(groups.length).to.be.above(0);
          expect(group).to.have.property('is_public', true);
          expect(response.body.meta.total).to.eql(2);
          done();
        });
    });
  });

  describe('Group Membership', () => {
    describe('Getting a List of Groups: Get /api/v1/groups', () => {
      it('should successfully return a list of groups the user is a member of', (done) => {
        Agent
          .get('/api/v1/groups')
          .set('Authorization', `Bearer ${adminToken}`)
          .then((response: any) => {
            const listing = response.body.data;
            const group = _.sample(listing);
            const validation = GroupDetailsSchema.validate(group);

            expect(response).to.have.status(200);
            expect(listing).to.be.an('array');
            expect(listing.length).to.be.above(0);
            expect(validation.error).to.be.null;
            expect(
              _.find(group.members, id => id === amdinUser._id)
            ).to.not.be.undefined;
            done();
          });
      });

      it('should return empty list if user is not a member of a group', (done) => {
        Agent
          .get(`/api/v1/groups`)
          .set('Authorization', `Bearer ${user1Token}`)
          .then((response: any) => {
            const listing = response.body.data;

            expect(response).to.have.status(200);
            expect(listing).to.be.an('array');
            expect(listing.length).to.equal(0);
            done();
          });
      });
    });

    describe('Getting a Single Group: GET /api/v1/groups/:groupId', () => {
      it('should return a single group the user is a member of', (done) => {
        Agent
          .get(`/api/v1/groups/${staticGroups[0]._id}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .then((response: any) => {
            const group = response.body.data;
            const validation = GroupDetailsSchema.validate(group);

            expect(response).to.have.status(200);
            expect(validation.error).to.be.null;
            expect(
              _.find(group.members, id => id === amdinUser._id)
            ).to.not.be.undefined;
            done();
          });
      });

      it('should be null if user is not a member of the requested group', (done) => {
        Agent
          .get(`/api/v1/groups/${testGroup1._id}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .then((response: any) => {
            expect(response).to.have.status(200);
            expect(response.body.data).to.be.null;
            done();
          });
      });
    });
  });

  describe('Group User Permissions & Actions', () => {
    describe('User & Contributor Role', () => {
      it('should allow group creation by any user', (done) => {
        Agent
          .post('/api/v1/groups')
          .set('Authorization', `Bearer ${contributorToken}`)
          .send(testGroup2)
          .then((response: any) => {
            const newGroup = response.body.data;
            expect(response).to.have.status(201);
            expect(newGroup).to.have.property('name', testGroup2.name);
            expect(newGroup).to.have.property('is_public', testGroup2.is_public);
            expect(newGroup).to.have.property('owner', testGroup2.owner);
            expect(newGroup.members).to.be.an('array');
            expect(newGroup.members.length).to.be.above(0);
            done();
          });
      });

      it('should allow a user to see a group if they are a member of the group', (done) => {
        Agent
          .get(`/api/v1/groups/${staticGroups[1]._id}`)
          .set('Authorization', `Bearer ${contributorToken}`)
          .then((response: any) => {
            const group = response.body.data;
            expect(response).to.have.status(200);
            expect(group.members.find((member: any) => member._id === user1._id)).to.not.eql('undefined');
            done();
          });
      });

      it('should allow a user to update a group if they user own it', (done) => {
        Agent
          .patch(`/api/v1/groups/${testGroup2._id}`)
          .set('Authorization', `Bearer ${contributorToken}`)
          .send({
            name: 'Addison Gamers'
          })
          .then(response => {
            expect(response).to.have.status(204);
            return (
              Agent
                .get(`/api/v1/groups/${testGroup2._id}`)
                .set('Authorization', `Bearer ${contributorToken}`)
            );
          })
          .then(response => {
            expect(response.body.data).to.have.property('name', 'Addison Gamers');
            done();
          });
      });

      it('should disallow a user from seeing a group they are not a member of', (done) => {
        Agent
          .get(`/api/v1/groups/${staticGroups[1]._id}`)
          .set('Authorization', `Bearer ${user1Token}`)
          .catch((response: any) => {
            expect(response).to.have.status(403);
            done();
          });
      });

      it('should disallow a user from edting a group they do not own', (done) => {
        Agent
          .patch(`/api/v1/groups/${staticGroups[1]._id}`)
          .set('Authorization', `Bearer ${contributorToken}`)
          .send({
            name: 'Addison Gamers'
          })
          .catch((response: any) => {
            expect(response).to.have.status(403);
            done();
          });
      });
    });

    describe('Admin Role', () => {
      it('should allow admin to see a group even when lacking membership', (done) => {
        Agent
          .get(`/api/v1/groups/${testGroup2._id}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .then((response: any) => {
            const newGroup = response.body.data;
            expect(response).to.have.status(200);
            expect(newGroup).to.have.property('name', 'Addison Gamers');
            expect(newGroup).to.have.property('is_public', testGroup2.is_public);
            expect(newGroup).to.have.property('owner', testGroup2.owner);
            expect(newGroup.members).to.be.an('array');
            done();
          });
      });

      it('should allow admin to update a group even when lacking ownership', (done) => {
        Agent
          .patch(`/api/v1/groups/${testGroup2._id}`)
          .set('Authorization', `Bearer ${adminToken}`)
          .send({
            name: 'Another Name I Want'
          })
          .then(response => {
            expect(response).to.have.status(204);
            return (
              Agent
                .get(`/api/v1/groups/${testGroup2._id}`)
                .set('Authorization', `Bearer ${adminToken}`)
            );
          })
          .then(response => {
            expect(response.body.data).to.have.property('name', 'Another Name I Want');
            done();
          });
      });
    });
  });
});
