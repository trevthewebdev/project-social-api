import { Request, Response } from 'express';
import * as _ from 'lodash';

import { normalizeResponseData } from '../utils/dataUtils';

import Events from './../models/eventModel';
import logger from '../utils/logUtils';

// import { IEventModel } from './../models/eventModel';
// import { IEventListing, IEventDetails, IEventCreate } from './../types/eventTypes';
// import { validateId } from './../validators/shared/customValidators';
import {
  EventCreateSchema,
  EventCreateSchemaTest,
  EventUpdateSchema
} from './../validators/eventValidators';
import { catchAndRespond } from './methods/global';

const log = logger.child({ feature: 'events' });

export function get(req: Request, res: Response) {
  Events
    .find({}, { __v: 0 })
    .then(events => {
      res.status(200).json({
        data: normalizeResponseData(events)
      });
    })
    .catch(err => res.boom.badImplementation(err));
}

export function getOne(req: Request, res: Response) {
  Events
    .findOne({ _id: req.params.id }, { __v: 0 })
    .then(event => res.status(200).json({ data: event || null }))
    .catch((err) => res.boom.badImplementation(err));
}

export function post(req: Request, res: Response) {
  Events
    .createWithDetails(req.body)
    .then(newDetails => res.status(201).json({ data: newDetails }))
    .catch(catchAndRespond(req, res, log));
}

export function update(req: Request, res: Response) {
  Events
    .findByIdAndUpdate(req.params.id, { $set: req.body })
    .then(result => {
      if (!result) {
        return Promise.reject({
          type: 'not found'
        });
      }

      res.status(204).json();
    })
    .catch(err => {
      switch (err.type) {
        case 'not found':
          break;

        default:
          res.boom.badImplementation(err);
      }
    });
}

export function remove(req: Request, res: Response) {
  Events
    .findByIdAndRemove({ _id: req.params.id })
    .then(result => {
      if (!result) {
        return Promise.reject({
          type: 'not found'
        });
      }

      res.status(204).json();
    })
    .catch(err => {
      switch (err.type) {
        case 'not found':
          res.status(404).json();
          break;

        default:
          res.boom.badImplementation(err);
      }
    });
}
