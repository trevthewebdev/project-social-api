import * as Morgan from 'morgan';
import * as BodyParser from 'body-parser';
import * as Boom from 'express-boom';
import * as cors from 'cors';
import config from '../config';

import authMiddleware from './authMiddleware';
import { requestLogger } from '../utils/logUtils';
import { attachRequestId } from './request';

const auth = authMiddleware();

export default function(app: any) {
  if (config.NODE_ENV === 'development') {
    app.use(Morgan('dev'));
  }

  // app.use(requestLogger);
  app.use(cors());
  app.use(auth.initialize());
  app.use(attachRequestId);
  app.use(BodyParser.urlencoded({ extended: true }));
  app.use(BodyParser.json());
  app.use(Boom());
}
