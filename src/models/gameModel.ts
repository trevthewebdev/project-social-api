import { Request, Response } from 'express';
import * as Mongoose from 'mongoose';
import { model, Schema } from 'mongoose';

import { IGameDocument, IGameModel } from '../types/gameTypes';

const GameSchema = new Mongoose.Schema({
  name: {
    type:           String,
    required:       true,
    unique:         true
  },
  slug: {
    type:           String,
    unique:         true
  },
  bgg_id: {
    type:           String
  },
  bgg_rank:         Number,
  categories: [{
    type:           Schema.Types.ObjectId,
    ref:            'Category'
  }],
  edition:          Number,
  expansions:       [{
    type:           Schema.Types.ObjectId,
    ref: 'Game'
  }],
  is_archived:      Boolean,
  is_expansion:     Boolean,
  player_interaction: [{
    type:           String,
    default:        'competitive'
  }],
  parent_game: {
    type:           Schema.Types.ObjectId,
    ref:            'Game'
  },
  primary_mechanic: String,
  mechanics:        [String],
  min_duration:     Number,
  max_duration:     Number,
  player_min:       Number,
  player_max:       Number,
  player_best:      Number,
  primary_category: {
    type:           Schema.Types.ObjectId,
    ref:            'Category'
  },
  description:      String,
  theme:            String,
  weight: {
    type:           Number,
    min:            1,
    max:            5
  },
  media: {
    images:         [ String ],
    videos:         [ String ],
    downloads:      [ String ]
  },
  rules: {
    type:           Schema.Types.ObjectId,
    ref:            'Rule'
  },
  owned_by: [{
    type:           Schema.Types.ObjectId,
    ref:            'User'
  }],
  created_at: {
    type:           Date,
    require:        true,
    default:        Date.now
  },
  updated_at: {
    type:           Date,
    require:        true,
    default:        Date.now
  }
});

GameSchema.statics.findWithCount = function findWithCount(req: Request, res: Response) {
  const {
    fieldsDBQuery,
    filterDBQuery,
    populateDBQuery,
  } = res.locals;

  return Promise.all([
    this
      .find(filterDBQuery, fieldsDBQuery.games)
      .populate(populateDBQuery)
      .limit(parseInt(req.query.limit, 10) || 100)
      .skip(parseInt(req.query.skip, 10) || 0)
      .sort({}),
    this.find(filterDBQuery).count()
  ]);
};

export default model<IGameDocument, IGameModel>('Game', GameSchema);
