import * as Chai from 'chai';
import { expect } from 'chai';
const chaiHttp = require('chai-http');

import server from './../../src/server';
import {
  LocationDetailsSchema
} from './../../src/validators/locationValidators';
import {
  staticLocations,
  testLocation1,
  testLocation2
} from '../../seed/data/locationSeedData';
import { staticUsers } from '../../seed/data/userSeedData';

Chai.use(chaiHttp);

const Agent = Chai.request.agent(server);

describe('Locations', () => {
  const adminUser = staticUsers[0];
  const user1 = staticUsers[1];
  const user2 = staticUsers[3];
  let adminToken: string;
  let user1Token: string;
  let user2Token: string;

  before(function(done: (error?: any) => void) {
    this.timeout(6000);

    const authAdmin = new Promise((resolve, reject) => {
      Agent
        .post('/api/v1/authenticate')
        .send({
          email: adminUser.local.email,
          password: adminUser.local.password
        })
        .then(response => resolve(response.body.data.token))
        .catch(error => reject(error));
    });
    const authUser1 = new Promise((resolve, reject) => {
      Agent
        .post('/api/v1/authenticate')
        .send({
          email: user1.local.email,
          password: user1.local.password
        })
        .then(response => resolve(response.body.data.token))
        .catch(error => reject(error));
    });
    const authUser2 = new Promise((resolve, reject) => {
      Agent
        .post('/api/v1/authenticate')
        .send({
          email: user2.local.email,
          password: user2.local.password
        })
        .then(response => resolve(response.body.data.token))
        .catch(error => reject(error));
    });

    Promise
      .all([ authAdmin, authUser1, authUser2 ])
      .then((tokens: any[]) => {
        adminToken = tokens[0];
        user1Token = tokens[1];
        user2Token = tokens[2];
        done();
      })
      .catch(error => console.log(error));
  });

  // Adding
  describe('Adding Locations: POST /api/v1/locations', () => {
    it('should successfully add a location', (done) => {
      Agent
        .post('/api/v1/locations')
        .set('Authorization', `Bearer ${adminToken}`)
        .send(testLocation1)
        .then((response: any) => {
          const newLocation = response.body.data;

          expect(response).to.have.status(201);
          expect(newLocation).to.have.property('name', testLocation1.name);
          expect(newLocation).to.have.property('street', testLocation1.street);
          // expect(newLocation).to.have.property('suite', testLocation1.suite);
          expect(newLocation).to.have.property('city', testLocation1.city);
          expect(newLocation).to.have.property('state', testLocation1.state);
          expect(newLocation).to.have.property('zip', testLocation1.zip);
          expect(newLocation).to.have.property('lat', testLocation1.lat);
          expect(newLocation).to.have.property('long', testLocation1.long);
          expect(newLocation).to.have.property('owned_by', testLocation1.owned_by);
          done();
        });
    });

    it('should successfully add a location with the same name different user without conflict', (done) => {
      Agent
        .post('/api/v1/locations')
        .send(testLocation2)
        .set('Authorization', `Bearer ${adminToken}`)
        .then((response: any) => {
          const newLocation = response.body.data;

          expect(response).to.have.status(201);
          expect(newLocation).to.have.property('name', testLocation2.name);
          expect(newLocation).to.have.property('street', testLocation2.street);
          expect(newLocation).to.not.have.property('suite');
          expect(newLocation).to.have.property('city', testLocation2.city);
          expect(newLocation).to.have.property('state', testLocation2.state);
          expect(newLocation).to.have.property('zip', testLocation2.zip);
          expect(newLocation).to.have.property('lat', testLocation2.lat);
          expect(newLocation).to.have.property('long', testLocation2.long);
          expect(newLocation).to.have.property('owned_by', testLocation2.owned_by);
          done();
        });
    });

    it('should fail to add a location with a conflict', (done) => {
      Agent
        .post('/api/v1/locations')
        .set('Authorization', `Bearer ${adminToken}`)
        .send(testLocation1)
        .catch((error: Error) => {
          expect(error).to.have.status(409);
          done();
        });
    });

    it('should fail to add a location with invalid payload', (done) => {
      Agent
        .post('/api/v1/locations')
        .set('Authorization', `Bearer ${adminToken}`)
        .send({
          name: 'Invalid Payload'
        })
        .catch((error: Error) => {
          expect(error).to.have.status(400);
          done();
        });
    });
  });

  describe('Getting Locations: GET /api/v1/locations', () => {
    it('should successfully return a list of locations owned by the user', (done) => {
      Agent
        .get('/api/v1/locations')
        .set('Authorization', `Bearer ${user1Token}`)
        .then(response => {
          const listing = response.body.data;
          const location = listing[0];
          const validation = LocationDetailsSchema.validate(location);

          expect(response).to.have.status(200);
          expect(listing).to.be.an('array');
          expect(listing.length).to.be.above(0);
          expect(validation.error).to.be.null;
          expect(location).to.have.property('owned_by', user1._id);
          done();
        });
    });

    it('should return a single location owned by the a user', (done) => {
      Agent
        .get(`/api/v1/locations/${staticLocations[1]._id}`)
        .set('Authorization', `Bearer ${adminToken}`)
        .then(response => {
          const location = response.body.data;
          const validation = LocationDetailsSchema.validate(location);

          expect(response).to.have.status(200);
          expect(location).to.be.an('object');
          expect(location).to.have.property('owned_by', adminUser._id);
          expect(validation.error).to.be.null;
          done();
        });
    });

    it('should return a no data if user does not own the location', (done) => {
      Agent
        .get(`/api/v1/locations/${staticLocations[1]._id}`)
        .set('Authorization', `Bearer ${user2Token}`)
        .then(response => {
          expect(response).to.have.status(200);
          expect(response.body.data).to.be.null;
          done();
        });
    });
  });

  describe('Updating Locations: PATCH /api/v1/locations/:id}', () => {
    const locationUpdates = {
      name: '2824 Rolling Hills',
      street: '2824 Rolling Hills, Flower Mound, TX 75022',
      city: 'Flower Mound',
      state: 'tx',
      zip: '75022',
      lat: 33.032285,
      long: -97.149732
    };

    it('should successfully update a location a user owns with all properties possible', (done) => {
      Agent
        .patch(`/api/v1/locations/${staticLocations[2]._id}`)
        .set('Authorization', `Bearer ${user1Token}`)
        .send(locationUpdates)
        .then(response => {
          expect(response).to.have.status(204);
          done();
        });
    });

    it('should fail update a location the a user doesn\'t own returning a 403', (done) => {
      Agent
        .patch(`/api/v1/locations/${staticLocations[0]._id}`)
        .set('Authorization', `Bearer ${user1Token}`)
        .send(locationUpdates)
        .catch(response => {
          expect(response).to.have.status(403);
          done();
        });
    });

    it('should fail to update a location with an invalid payload returning a 400', (done) => {
      Agent
        .patch(`/api/v1/locations/${staticLocations[0]._id}`)
        .set('Authorization', `Bearer ${user1Token}`)
        .send({ zip: 12345 })
        .catch(response => {
          expect(response).to.have.status(400);
          done();
        });
    });

    it('should fail to update a location if "owned_by" was included in payload, returning a 400', (done) => {
      Agent
        .patch(`/api/v1/locations/${staticLocations[0]._id}`)
        .set('Authorization', `Bearer ${user1Token}`)
        .send({
          name: 'The New Name',
          owned_by: adminUser._id
        })
        .catch(error => {
          expect(error).to.have.status(400);
          expect(error.response.body)
            .to.have.property('message', 'child \"owned_by\" fails because [\"owned_by\" is not allowed]');
          done();
        });
    });

    xit('should successfully update any location as an admin');
  });

  describe('Removing Locations: DELETE /api/v1/locations/{id:name}', () => {
    it('should successfully delete a location a user owns', (done) => {
      Agent
        .del(`/api/v1/locations/${staticLocations[2]._id}`)
        .set('Authorization', `Bearer ${user1Token}`)
        .then(response => {
          expect(response).to.have.status(204);
          done();
        });
    });

    it('should fail delete a location the a user doesn\'t own returning a 403', (done) => {
      Agent
        .del(`/api/v1/locations/${staticLocations[0]._id}`)
        .set('Authorization', `Bearer ${user1Token}`)
        .catch(error => {
          expect(error).to.have.status(403);
          done();
        });
    });

    xit('should successfully delete any location as an admin');
  });
});
