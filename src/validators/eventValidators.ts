import * as Mongoose from 'mongoose';
import * as Joi from 'joi';

import { Validator } from '../types/genericTypes';
import {
  IParentEventDetails,
  IEventUpdate,
  IEventCreate,
} from '../types/eventTypes';

const locationSchemaMap = {
  coorinates: Joi.array().items(Joi.number()).max(2),
  address1: Joi.string(),
  address2: Joi.string(),
  country: Joi.string(),
  postal_code: Joi.string(),
  state: Joi.string()
};

const eventCreateSchemaMap: Validator<IEventCreate> = {
  attendees: Joi.array().items(
    Joi.object({
      user: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      role: Joi.string().valid(['req-participant', 'opt-participant']),
      status: Joi.string().valid(['accepted', 'tenative', 'declined']),
    })
  ),
  organizer: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
  group: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  loc: Joi.object({
    coordinates: Joi.array().items(Joi.number()).max(2).required(),
    country: Joi.string().required(),
    postal_code: Joi.string(),
    state: Joi.string().required(),
    address_1: Joi.string(),
    address_2: Joi.string(),
    city: Joi.string()
  }).required(),
  public: Joi.boolean(),
  from: Joi.number().required(),
  to: Joi.number().required(),
  tz: Joi.string().required(),
  status: Joi.string().valid(['confirmed', 'tenative', 'cancelled']),
  repeats: Joi.boolean(),
};

export const EventCreateSchema = Joi.object(eventCreateSchemaMap);
export const EventCreateSchemaTest = EventCreateSchema.keys({
  _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
});

const eventUpdateSchemaMap: Validator<IEventUpdate> = Object.assign({}, eventCreateSchemaMap, {
  owner: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  group: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  loc: Joi.object(locationSchemaMap).optional()
});

export const EventUpdateSchema = Joi.object(eventUpdateSchemaMap);

// const eventDetailsSchemaMap: Validator<IParentEventDetails> = {
//   attendees: Joi.array().items(
//     Joi.object({
//       user: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
//       role: Joi.string().valid(['req-participant', 'opt-participant']),
//       status: Joi.string().valid(['accepted', 'tenative', 'declined']),
//     })
//   ),
//   organizer: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
//   group: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
//   loc: Joi.object({
//     coorinates: Joi.array().items(Joi.number()).max(2),
//     address1: Joi.string(),
//     address2: Joi.string(),
//     country: Joi.string().required(),
//     postal_code: Joi.string(),
//     state: Joi.string().required()
//   }),
//   public: Joi.boolean(),
//   from: Joi.number(),
//   to: Joi.number(),
//   tz: Joi.string(),
//   status: Joi.string().valid(['confirmed', 'tenative', 'cancelled']),
//   repeats: Joi.boolean(),
//   created_at: Joi.date(),
//   update_at: Joi.date().optional(),
// };

// export const EventDetailsSchema = Joi.object(eventDetailsSchemaMap);
