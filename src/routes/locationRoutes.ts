import * as express from 'express';

import { validateRequest } from '../validators/shared/customValidators';
import { LocationCreateSchema, LocationUpdateSchema } from '../validators/locationValidators';
import authMiddleware from '../middleware/authMiddleware';
import {  } from '../middleware/';

import {
  get,
  getOne,
  post,
  update,
  remove,
} from './../controllers/locationController';

const Router = express.Router();
const auth = authMiddleware();

Router.use(auth.authenticate());

Router.post('/', validateRequest(LocationCreateSchema), post);
Router.get('/',  get);
Router.get('/:id', getOne);
Router.patch('/:id', validateRequest(LocationUpdateSchema), update);
Router.delete('/:id', remove);

export default Router;
