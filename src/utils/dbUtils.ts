// Types
import { UserDetails } from '../../src/types/userTypes';

// Functions
import { seedCollection } from './seedUtils';

// Models
import Events from '../../src/models/eventModel';
import Games from '../../src/models/gameModel';
import Groups from '../../src/models/groupModel';
import Locations from '../../src/models/locationModel';
import Users from '../../src/models/userModel';

import EventSeedData from '../../seed/data/eventSeedData';
import GameSeedData from '../../seed/data/gameSeedData';
import UserSeedData from '../../seed/data/userSeedData';
import GroupSeedData from '../../seed/data/groupSeedData';
import LocationSeedData from '../../seed/data/locationSeedData';

const resources: { [key: string]: any } = {
  events: {
    model: Events,
    data: EventSeedData
  },
  games: {
    model: Games,
    data: GameSeedData
  },
  groups: {
    model: Groups,
    data: GroupSeedData
  },
  locations: {
    model: Locations,
    data: LocationSeedData
  },
  users: {
    model: Users,
    data: UserSeedData
  }
};

/**
 * Drop Specific
 * Drops any number of collections
 * @param collections string[] Array of collection names
 */
export function dropSpecifc(collections: string[]) {
  const promises = collections.map(
    (collectionName: string) => (
      resources[collectionName]['model']
        .remove()
        .exec()
        .catch((error: Error) => {
          console.log('errors happened: ', error);
        })
    )
  );

  return Promise.all(promises);
}

/**
 * Drop All
 * Removes all records from all collections in the database
 */
export function dropAll() {
  const promises = (
    Object
      .keys(resources)
      .map(collectionName => (
        resources[collectionName]['model']
          .remove()
          .exec()
      ))
  );

  return Promise.all(promises);
}

/**
 * Drop and Seed Specific
 * Drops any number of collections and re-populates the collections with seed data
 * @param collections string[] Array of collection names
 */
export function dropAndSeedSpecific(collections: string[]) {
  return (
    dropSpecifc(collections)
      .then(() => Promise.map(collections, collectionName => (
        seedCollection(
          resources[collectionName]['model'],
          resources[collectionName]['data'],
          collectionName
        )()
      )))
      .then(promises => Promise.all(promises))
      .catch(error => {
        return Promise.reject(`Drop And Seed Specifc: ${error.message}`);
      })
  );
}

/**
 * Drop and Seed All
 * Basically just reseed the database
 */
export function dropAndSeedAll() {
  return (
    dropAll()
      .then(() => (
        Object
          .keys(resources)
          .map(collectionName => (
            seedCollection(
              resources[collectionName]['model'],
              resources[collectionName]['data'],
              collectionName
            )
          ))
      ))
      .catch(error => {
        return Promise.reject(`Drop And Seed Specifc: ${error.message}`);
      })
  );
}

export function authenticateTestUser(Agent: any, userObj: UserDetails): Promise<string> {
  return new Promise((resolve, reject) => {
    Agent
      .post('/v1/authenticate')
      .send({
        email: userObj.local.email,
        password: userObj.local.password
      })
      .then((response: any) => resolve(response.body.data.token))
      .catch((error: any) => {
        console.log('made it here: ', userObj);
        reject(error);
      });
  });
}
