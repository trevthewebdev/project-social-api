import * as Chai from 'chai';
import { expect } from 'chai';
import * as Moment from 'moment';
import * as _ from 'lodash';

// server & utils
import server from '../../../src/server';
import { authenticateTestUser } from '../utils/authTestUtils';
import { dropSpecifc, dropAndSeedSpecific } from '../../../src/utils/dbUtils';

// types & validators

// data
import { staticUsers, trevor, mark, luke } from '../../../seed/data/userSeedData';
import { mcKinneyKnights } from '../../../seed/data/groupSeedData';
import { staticEvents } from '../../../seed/data/eventSeedData';

Chai.use(require('chai-http'));

const Agent = Chai.request.agent(server);

const baseUrl = '/api/v1/events';
const baseUser = staticUsers.find(({ role }) => role === 'user');
const contributorUser = staticUsers.find(({ role }) => role === 'contributor');
const adminUser = staticUsers.find(({ role }) => role === 'admin');
const superAdminUser = staticUsers.find(({ role }) => role === 'superadmin');
const parentEvent = _.omit(staticEvents[0], ['_id']);

describe('POST: Events', () => {
  let baseUserToken: string;
  let contributorToken: string;
  let adminToken: string;
  let superAdminToken: string;

  before(function(done: (error?: any) => void) {
    dropAndSeedSpecific(['users', 'events'])
      .then(() => Promise.all([
        authenticateTestUser(Agent, baseUser),
        authenticateTestUser(Agent, contributorUser),
        authenticateTestUser(Agent, adminUser),
        authenticateTestUser(Agent, superAdminUser)
      ]))
      .spread((bToken: string, cToken: string, aToken: string, sToken: string) => {
        baseUserToken = bToken;
        contributorToken = cToken;
        adminToken = aToken;
        superAdminToken = sToken;
        done();
      })
      .catch(error => done(error));
  });

  beforeEach(function(done: (error?: any) => void) {
    dropSpecifc(['events']).then(() => done());
  });

  describe('Creating Events', () => {
    const expectedParentEvent = Object.assign({}, parentEvent);
    delete expectedParentEvent.attendees;
    delete expectedParentEvent.loc;

    it.only('should allow a General User to create an event', done => {
      Agent
        .post(baseUrl)
        .set('Authorization', `Bearer ${baseUserToken}`)
        .send(parentEvent)
        .then(response => {
          expect(response).to.have.status(201);
          expect(response.body.data).to.include(expectedParentEvent);
          done();
        })
        .catch(error => {
          console.log(error.message);
          done(error);
        });
    });

    it('should allow a Contributor to create an event', done => {
      Agent
        .post(baseUrl)
        .set('Authorization', `Bearer ${contributorToken}`)
        .send(parentEvent)
        .then(response => {
          expect(response).to.have.status(201);
          expect(response.body.data).to.deep.equal(parentEvent);
          done();
        })
        .catch(error => done(error));
    });

    it('should allow a Admin to create an event', done => {
      Agent
        .post(baseUrl)
        .set('Authorization', `Bearer ${adminToken}`)
        .send(parentEvent)
        .then(response => {
          expect(response).to.have.status(201);
          expect(response.body.data).to.deep.equal(parentEvent);
          done();
        })
        .catch(error => done(error));
    });

    it('should allow a Super Admin to create an event', done => {
      Agent
        .post(baseUrl)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .send(parentEvent)
        .then(response => {
          expect(response).to.have.status(201);
          expect(response.body.data).to.deep.equal(parentEvent);
          done();
        })
        .catch(error => done(error));
    });
  });
});
