import * as Mongoose from 'mongoose';
import * as Joi from 'joi';
import {
  Validator,
} from '../types/genericTypes';

import {
  IPlayDetails,
  IPlayUpdate,
  IPlayCreate,
  PlayBase
} from '../types/playTypes';

// Base
const playBaseSchemaMap: Validator<PlayBase> = {
  duration: Joi.number().integer().required(),
  date_played: Joi.date().iso().required(),
  kind: Joi.string().valid('competitive', 'coop', 'semi-coop').required()
};

export const PlayBaseSchema = Joi.object(playBaseSchemaMap);

// Create
const playCreateSchemaMap: Validator<IPlayCreate> = Object.assign({}, playBaseSchemaMap, {
  game: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  players: Joi.array().items(
    Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  )
});

export const PlayCreateSchema = PlayBaseSchema.keys(playCreateSchemaMap);
export const PlayCreateSchemaTest = PlayCreateSchema.keys({
  _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
});

// Update
const playUpdateSchemaMap: Validator<IPlayUpdate> = Object.assign({}, playBaseSchemaMap, {
  // first_name: Joi.string().max(50, 'utf8').optional(),
  // last_name: Joi.string().optional(),
  // local: Joi.object({
  //   email: Joi.string().optional(),
  //   password: Joi.string().forbidden()
  // }),
  // game_stats: Joi.object({
  //   total_played: Joi.number(),
  //   won: Joi.number(),
  //   lost: Joi.number()
  // }),
  // games: Joi.array().items(Joi.string()),
  // groups: Joi.array().items(Joi.string()),
  // permissions: Joi.number()
});

export const PlayUpdateSchema = PlayBaseSchema.keys(playUpdateSchemaMap);

// Details

const playDetailsSchemaMap: Validator<IPlayDetails> = Object.assign({}, playBaseSchemaMap, {
  game: Joi.object({
    _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
    name: Joi.string()
  }),
  players: Joi.array().items(Joi.object()) // might be able to use user validators here as one of the items
});

export const PlayDetailsSchema = PlayBaseSchema.keys(playDetailsSchemaMap);
