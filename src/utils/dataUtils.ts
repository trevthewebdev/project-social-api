export function normalizeResponseData(results: any[] | null): any[] {
  return (!results || !results.length) ? [] : results;
}
