import * as Faker from 'faker';
import { IUserDetails } from './../../src/types/userTypes';
import { staticUsers } from './userSeedData';

const trevor = staticUsers[0];
const shannyn = staticUsers[1];
const mark = staticUsers[2];
const luke = staticUsers[3];
const david = staticUsers[4];

function generateGroups(count: number) {
  const groups: Partial<any>[] = [];
  while (--count) {
    groups.push({
      name: Faker.random.words(),
      loc: {
        coordinates: [
          Faker.address.longitude(),
          Faker.address.latitude()
        ],
        state: Faker.address.stateAbbr()
      },
      is_public: true,
      owner: mark._id,
      members: [
        mark._id
      ]
    });
  }
  return groups;
}

export const mcKinneyKnights = {
  _id: '5a470aec08a5ded598dda12c',
  name: 'McKinney Knights',
  owner: trevor._id,
  is_public: false,
  members: [
    trevor._id,
    mark._id,
    luke._id
  ],
  loc: {
    coordinates: [-96.640041, 33.196645],
    state: 'TX'
  }
};

export const staticGroups = [
  mcKinneyKnights,
  {
    _id: '5a470af508a5ded598dda12d',
    name: 'McKinney Gamers Without Trevor',
    owner: '593a2c3f5ee35676febbbfa0',
    is_public: false,
    members: [
      '593a2c3f5ee35676febbbfa0',
      '593a2c465ee35676febbbfa1',
      '5abfa484a70a874a70b3a144'
    ],
    loc: {
      coordinates: [-96.640041, 33.196645],
      state: 'TX'
    }
  },
  {
    _id: '5a9bb99c4dbb539ccb110c1e',
    name: 'Downtown Dallas Gamers',
    is_public: true,
    owner: '593a2c3f5ee35676febbbfa0',
    members: [
      '593a2c3f5ee35676febbbfa0'
    ],
    loc: {
      coordinates: [-96.800258, 32.779071],
      state: 'TX'
    }
  },
  {
    _id: '5a9bb9ba4dbb539ccb110c1f',
    name: 'Allen Texas Gamers',
    is_public: true,
    owner: '593a2c465ee35676febbbfa1',
    members: [
      '593a2c465ee35676febbbfa1',
      '593a2c3f5ee35676febbbfa0',
      '593a2c245ee35676febbbf9e'
    ],
    loc: {
      coordinates: [-96.670461, 33.103255],
      state: 'TX'
    }
  }
];

export const testGroup1 = {
  _id: '5a987d567b75ba6647919745',
  name: 'Downtown Gamers',
  is_public: false,
  owner: '593a2c3f5ee35676febbbfa0',
  members: [
    '593a2c3f5ee35676febbbfa0'
  ],
  loc: {
    coordinates: [-96.800258, 32.779071],
    state: 'TX'
  }
};

export const testGroup2 = {
  _id: '5a987d6b7b75ba6647919746',
  name: 'Allen Gamers',
  is_public: true,
  owner: '5abfa484a70a874a70b3a144',
  members: [
    '5abfa484a70a874a70b3a144',
    '593a2c3f5ee35676febbbfa0',
    '593a2c245ee35676febbbf9e'
  ],
  loc: {
    coordinates: [-96.670461, 33.103255],
    state: 'TX'
  }
};

export default [
  ...staticGroups,
  ...generateGroups(8)
];
