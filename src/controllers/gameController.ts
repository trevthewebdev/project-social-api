import { Request, Response } from 'express';
import * as _ from 'lodash';

import logger from '../utils/logUtils';
import MetaError from '../utils/errorUtils';

import Games from '../models/gameModel';
import {
  IGameListing,
  IGameDetails
} from '../types/gameTypes';

import {
  catchAndRespond,
  checkAlreadyExists,
  findOneResource,
  saveDocument
} from './methods/global';

import {
  generateAndAssignSlug,
  updateParentGame
} from './methods/games';

import {
  GAME_INCLUDES_SCHEMA,
  generateSerializer
} from '../serializers';

const log = logger.child({ feature: 'games' });

export function get(req: Request, res: Response) {
  const { parsedIncludes, parsedFields } = res.locals.query;

  Games
    .findWithCount(req, res)
    .spread((games: any, count: number) => {
      const serializer = generateSerializer(GAME_INCLUDES_SCHEMA, 'games', parsedIncludes, parsedFields);
      const { data, ...rest } = serializer.serialize(games);

      return {
        data: data.length ? data : null,
        ...rest,
        meta: {
          total: count,
          limit: parseInt(req.query.limit, 10) || 100,
          skip: parseInt(req.query.skip, 10) || 0
        }
      };
    })
    .then((result: any) => res.json(result))
    .catch(catchAndRespond(req, res, log));
}

export function getOne(req: Request, res: Response) {
  const { findBy } = req.app.locals;
  const query: any = {};

  query[findBy] = req.params.id;

  findOneResource(Games, query)
    .then((game: IGameDetails) => {
      if (!game) {
        return Promise.reject(
          new MetaError('Not Found', {
            type: 'not found'
          })
        );
      }

      res.status(200).json({
        data: game
      });
    })
    .catch(
      catchAndRespond(req, res, log)
    );
}

export function post(req: Request, res: Response) {
  const payload = req.body;
  Promise
    .all([
      findOneResource(Games, { name: payload.name })
        .then(checkAlreadyExists(payload)),
      findOneResource(Games, { _id: payload.parent_game })
    ])
    .then(generateAndAssignSlug)
    .then(saveDocument(Games, log))
    .then(updateParentGame)
    .then((newGame: any) => (
      res.status(201).json({
        data: _.omit(newGame.toJSON(), ['__v'])
      })
    ))
    .catch(
      catchAndRespond(req, res, log)
    );
}

export function update(req: Request, res: Response) {
  const payload = req.body;

  Games
    .findByIdAndUpdate(req.params.id, { $set: payload}, { new: true })
    .then(result => {
      if (!result) {
        return Promise.reject(
          new MetaError('Not Found', {
            type: 'not found'
          })
        );
      }
      res.status(204).json();
    })
    .catch(
      catchAndRespond(req, res, log)
    );
}

export function remove(req: Request, res: Response) {
  Games
    .findOneAndRemove({ _id: req.params.id })
    .then((result) => {
      if (!result) {
        return Promise.reject(
          new MetaError('Not Found', {
            type: 'not found'
          })
        );
      }
      res.status(204).json();
    })
    .catch(
      catchAndRespond(req, res, log)
    );
}
