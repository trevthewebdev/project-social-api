import * as joi from 'joi';
import { DelimiterValidatorFactory } from './../validators/shared/customValidators';

import { LisingRequestSchemaMap } from './shared/requestValidators';

import config from '../config';
import { Validator } from '../types/genericTypes';
import {
  GameBase,
  IGameCreate,
  IGameDetails,
  IGameUpdate
} from '../types/gameTypes';

const Joi = joi.extend(DelimiterValidatorFactory);

// Base
const gameBaseSchemaMap: Validator<GameBase> = {
  name: Joi.string().required(),
  slug: Joi.string(),
  owned_by: Joi.array().items(Joi.string())
};

export const GameBaseSchema = Joi.object(gameBaseSchemaMap);

// Joi.extend((joi: any) => ({
//   base: joi.string(),
//   name: 'mongoid',
//   rules: [
//     {
//       name: 'mongoid',
//       validate(params: any, value: string, state: any, options: any) {
//         const match = value.match(/^[0-9a-fA-F]{24}$/);
//         return (
//           (_.isArray(match))
//             ? value
//             : this.createError('mongoid', {}, state, options)
//         );
//       }
//     }
//   ]
// }));

const gameLisingRequestSchemaMap: Validator<any> = {
  filter: Joi.object({
    min_duration: Joi.string(),
    max_duration: Joi.string(),
    // min_player: Joi.string(),
    // max_player: Joi.string(),
    player_min: Joi.string(),
    player_max: Joi.string(),
    best_player: Joi.string(),
    min_weight: Joi.string(),
    max_weight: Joi.string(),
    is_expansion: Joi.boolean(),
    owned_by: Joi.array().delimeter().items(
      Joi.string()
    ),
    mechanics: Joi.array().delimeter().items(
      Joi.string()
    ),
    designers: Joi.array().delimeter().items(
      Joi.string()
    )
  }),
  fields: Joi.object({
    games: Joi.array().delimeter().items(
      Joi.string().valid([
        'name',
        'slug',
        'bgg_id',
        'bgg_rank',
        'categories',
        'edition',
        'expansions',
        'is_archived',
        'is_expansion',
        'player_interaction',
        'parent_game',
        'primary_mechanic',
        'mechanics',
        'min_duration',
        'max_duration',
        'player_min',
        'player_max',
        'player_best',
        'primary_category',
        'description',
        'theme',
        'weight',
        'owned_by'
      ])
    ).sparse(),
    users: Joi.array().delimeter().items(
      Joi.string().valid([
        'is_active',
        'local.email',
        'facebook',
        'twitter',
        'first_name',
        'last_name',
        'phone',
        'avatar',
        'groups',
        'game_stats',
        'role',
        'username',
        'zip_code',
        'tz'
      ])
    ).sparse()
  }),
  include: Joi.array().delimeter().items(
    Joi.string().valid([
      'parent_game',
      'owned_by'
    ])
  )
};

export const GameListingRequestSchema = Joi.object(
  Object.assign({}, LisingRequestSchemaMap, gameLisingRequestSchemaMap)
);

// Create
const gameCreateSchemaMap: Validator<IGameCreate> = Object.assign({}, gameBaseSchemaMap, {
  bgg_id: Joi.number(),
  categories: Joi.array().items(Joi.string()),
  description: Joi.string(),
  edition: Joi.number(),
  expansions: Joi.array(),
  is_expansion: Joi.boolean(),
  mechanics: Joi.array(),
  min_duration: Joi.number().min(0),
  max_duration: Joi.number().min(0),
  parent_game: Joi.string().regex(/^[0-9a-fA-F]{24}$/).when('is_expansion', {
    is: true,
    then: Joi.required()
  }),
  player_max: Joi.number(),
  player_min: Joi.number(),
  player_best: Joi.number(),
  player_interaction: Joi.array().items(
    Joi.string().valid('competitive', 'coop', 'semi-coop')
  ).required(),
  primary_category: Joi.string(),
  theme: Joi.string(),
  weight: Joi.number().min(1).max(5),
  owned_by: Joi.array().items(Joi.string()),
  media: Joi.object({
    downloads: Joi.array(),
    videos: Joi.array(),
    images: Joi.array()
  })
});

let GameCreateSchemaTemp = Joi.object(gameCreateSchemaMap);
if (config.env === 'test') {
  GameCreateSchemaTemp = GameCreateSchemaTemp.keys({
    _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  });
}

export const GameCreateSchema = GameCreateSchemaTemp;

// export GameCreateSchema;

// Update
const gameUpdateSchemaMap: Validator<IGameUpdate> = Object.assign({}, gameBaseSchemaMap, {
  name: Joi.string().optional(),
  bgg_id: Joi.number(),
  categories: Joi.array().items(Joi.string()),
  description: Joi.string(),
  edition: Joi.number(),
  expansions: Joi.array(),
  is_expansion: Joi.boolean(),
  mechanics: Joi.array(),
  min_duration: Joi.number().min(0),
  max_duration: Joi.number().min(0),
  parent_game: Joi.string().regex(/^[0-9a-fA-F]{24}$/).when('is_expansion', {
    is: true,
    then: Joi.required()
  }),
  player_max: Joi.number(),
  player_min: Joi.number(),
  player_best: Joi.number(),
  player_interaction: Joi.array().items(
    Joi.string().valid('competitive', 'coop', 'semi-coop')
  ),
  primary_category: Joi.string(),
  theme: Joi.string(),
  weight: Joi.number().min(1).max(5),
  owned_by: Joi.array().items(Joi.string()),
  media: Joi.object({
    downloads: Joi.array(),
    videos: Joi.array(),
    images: Joi.array()
  }),
  stats: Joi.object({
    total_plays: Joi.number(),
    recent_games: Joi.array().items(Joi.string())
  }),
});

export const GameUpdateSchema = GameBaseSchema.keys(gameUpdateSchemaMap);

// Details
const gameDetailsSchemaMap: Validator<IGameDetails> = Object.assign({}, gameBaseSchemaMap, {
  _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  name: Joi.string().optional(),
  slug: Joi.string(),
  bgg_id: Joi.number(),
  categories: Joi.array().items(Joi.string()),
  description: Joi.string(),
  edition: Joi.number(),
  expansions: Joi.array(),
  is_archived: Joi.boolean(),
  is_expansion: Joi.boolean(),
  mechanics: Joi.array(),
  min_duration: Joi.number().min(0),
  max_duration: Joi.number().min(0),
  parent_game: Joi.string().regex(/^[0-9a-fA-F]{24}$/).when('is_expansion', {
    is: true,
    then: Joi.required()
  }),
  player_max: Joi.number(),
  player_min: Joi.number(),
  player_best: Joi.number(),
  player_interaction: Joi.array().items(
    Joi.string().valid('competitive', 'coop', 'semi-coop')
  ),
  primary_category: Joi.string(),
  primary_mechanic: Joi.string(),
  bgg_rank: Joi.number(),
  theme: Joi.string(),
  weight: Joi.number().min(1).max(5),
  media: Joi.object({
    downloads: Joi.array(),
    videos: Joi.array(),
    images: Joi.array()
  }),

  stats: Joi.object({
    total_plays: Joi.number(),
    recent_games: Joi.array()
  }),

  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso().optional()
});

export const GameDetailsSchema = GameBaseSchema.keys(gameDetailsSchemaMap);
