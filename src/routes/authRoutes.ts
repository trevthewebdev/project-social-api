import * as express from 'express';
import { authenticate, resetPassword, requestResetPassword } from './../controllers/authController';
const Router = express.Router();

Router.post('/authenticate', authenticate);
Router.post('/password/reset', requestResetPassword);
Router.post('/password/confirm', resetPassword);

export default Router;
