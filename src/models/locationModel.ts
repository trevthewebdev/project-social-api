import * as Mongoose from 'mongoose';
import { model, Schema } from 'mongoose';

import { ILocationDocument, ILocationModel } from '../types/locationTypes';

const LocationSchema = new Mongoose.Schema({
  name:             String,
  street:           String,
  building_number:  Number,
  city:             String,
  state:            String,
  zip:              String,
  lat:              Number,
  long:             Number,
  owned_by: {
    type:           Schema.Types.ObjectId,
    ref:            'User'
  },
  created_at: {
    type:           Date,
    require:        true,
    default:        Date.now
  },
  updated_at: {
    type:           Date,
    require:        true,
    default:        Date.now
  }
});

export default model<ILocationDocument, ILocationModel>('Location', LocationSchema);
