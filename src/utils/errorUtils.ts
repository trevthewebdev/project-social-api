import { IMetaError } from '../types/ambient/error-meta';

function MetaErrorConstuctor(message: string, meta?: any): IMetaError {
  const instance: any = Reflect.construct(Error, [message, meta]);
  instance.meta = meta;
  Reflect.setPrototypeOf(instance, Reflect.getPrototypeOf(this));
  return instance;
}

MetaErrorConstuctor.prototype = Object.create(Error.prototype, {
  constructor: {
    value: Error,
    enumerable: false,
    writable: true,
    configurable: true
  }
});

Reflect.setPrototypeOf(MetaErrorConstuctor, Error);

const MetaError: any = MetaErrorConstuctor;

export default MetaError;
