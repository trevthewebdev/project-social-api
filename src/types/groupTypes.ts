/* This file will eventually be expanded to hold different countries addresses */
import { Request, Response } from 'express';
import { Document, Model } from 'mongoose';

export type GroupBase = {
  name:               string;
  is_public:             boolean;
  members:            string[];
  owner:              string;
};

export type GroupDetails = GroupBase & {
  avatar:             string;
  name:               string;
  loc: {
    coordinates:      number[];
    country:          string;
    postal_code:      string;
    state?:           string;
  };
};

export interface IGroupListing extends GroupBase {
  _id:                string;
}

export interface IGroupDetails extends GroupBase, GroupDetails {
  created_at?:        Date;
  updated_at?:        Date;
}

export type GroupCreate = GroupBase & Partial<GroupDetails> & {
  is_public?:            boolean;
};

export type GroupUpdate = Partial<GroupDetails> & {
  owner?:             string;
};

/* Mongoose Document & Model Types */
export interface IGroupDocument extends Document, IGroupDetails {}
export interface IGroupModel extends Model<IGroupDocument> {
  findSingleGroup: (req: Request, res: Response) => Promise<Partial<GroupDetails>>;
}
