import * as Mongoose from 'mongoose';
import * as Joi from 'joi';

import config from '../config';
import { Validator } from '../types/genericTypes';
import {
  UserBase,
  IUserCreate,
  IUserDetails,
  IUserUpdate
} from '../types/userTypes';

const userBaseSchemaMap: Validator<UserBase> = {
  is_active: Joi.boolean(),
  avatar: Joi.string().allow(null).uri({
    scheme: ['http', 'https']
  }).example('https://www.google.com'),
  first_name: Joi.string().max(50, 'utf8').required(),
  last_name: Joi.string().required(),
  phone: Joi.string(),
  username: Joi.string(),
  role: Joi.string().allow(['user', 'contributor', 'admin', 'superadmin']),
  zip_code: Joi.string().min(5).max(9),
  tz: Joi.string().example('America/Chicago')
};

export const UserBaseSchema = Joi.object(userBaseSchemaMap);

// Create
const userCreateSchemaMap: Validator<IUserCreate> = Object.assign({}, userBaseSchemaMap, {
  local: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required()
  }),
  username: Joi.string().required(),
  zip_code: Joi.string().min(5).max(9).required()
});

let UserCreateSchemaTemp = Joi.object(userCreateSchemaMap);
if (config.env === 'test') {
  UserCreateSchemaTemp = UserCreateSchemaTemp.keys({
    _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  });
}

export const UserCreateSchema = UserCreateSchemaTemp;

// Update
const userUpdateSchemaMap: Validator<IUserUpdate> = Object.assign({}, userBaseSchemaMap, {
  first_name: Joi.string().max(50, 'utf8').optional(),
  last_name: Joi.string().optional(),
  local: Joi.object({
    email: Joi.string().email().optional(),
    password: Joi.string().forbidden()
  }),
  game_stats: Joi.object({
    total_played: Joi.number(),
    won: Joi.number(),
    lost: Joi.number()
  }),
  groups: Joi.array().items(Joi.string()),
  permissions: Joi.number()
});

export const UserUpdateSchema = UserBaseSchema.keys(userUpdateSchemaMap);

// Details
const userDetailsSchemaMap: Validator<IUserDetails> = Object.assign({}, userBaseSchemaMap, {
  _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  local: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.any().forbidden()
  }),
  game_stats: Joi.object({
    total_played: Joi.number(),
    won: Joi.number(),
    lost: Joi.number()
  }),
  groups: Joi.array().items(Joi.string()),
  permissions: Joi.number()
});

export const UserDetailsSchema = UserBaseSchema.keys(userDetailsSchemaMap);
