import * as Joi from 'joi';
import { Request, Response } from 'express';
import * as StackParser from 'error-stack-parser';
import { MongoError } from 'mongodb';
import * as _ from 'lodash';

import logger from '../utils/logUtils';
import MetaError from '../utils/errorUtils';

import Locations from './../models/locationModel';
import {
  ILocationListing,
  ILocationDetails
} from './../types/locationTypes';

import {
  catchAndRespond,
  checkAlreadyExists,
  findOneResource,
  queryList,
  saveDocument
} from './methods/global';
import { unauthorized } from 'boom';

const log = logger.child({ feature: 'locations' });

export function get(req: Request, res: Response) {
  queryList(
    Locations,
    res.locals.dbQuery,
    { where: { owned_by: req.user._id } }
  )
    .then((results: any[]) => {
      const locations = results[0];
      const meta = results[1];

      res.status(200).json({
        data: locations,
        meta
      });
    })
    .catch(
      catchAndRespond(req, res, log)
    );
}

export function getOne(req: Request, res: Response) {
  const query: any = {
    _id: req.params.id,
    owned_by: req.user._id
  };

  findOneResource(Locations, query)
    .then((location: ILocationDetails) => {
      res.status(200).json({
        data: location
      });
    })
    .catch(
      catchAndRespond(req, res, log)
    );
}

export function post(req: Request, res: Response) {
  const payload = req.body;

  findOneResource(Locations, {
    $and: [
      { owned_by: payload.owned_by },
      { name: payload.name }
    ]
  })
    .then(checkAlreadyExists(payload))
    .then(saveDocument(Locations, log))
    .then((newLocation: any) => (
      res.status(201).json({
        data: _.omit(newLocation, ['__v'])
      })
    ))
    .catch(
      catchAndRespond(req, res, log)
    );
}

export function update(req: Request, res: Response) {
  const payload = req.body;

  Locations
    .findById(req.params.id)
    .then(location => (
      location
      ? Promise.resolve(location)
      : Promise.reject(
          new MetaError('Not Found', {
            type: 'not found'
          })
        )
    ))
    .then((location: any) => (
      location.owned_by.toString() === req.user._id.toString()
      ? Promise.resolve(location)
      : Promise.reject(
          new MetaError('forbidden', {
            type: 'forbidden'
          })
        )
    ))
    .then(location => Locations.update({ _id: req.params.id }, { $set: payload }))
    .then(result => res.status(204).json())
    .catch(catchAndRespond(req, res, log));
}

export function remove(req: Request, res: Response) {
  Locations
    .findById(req.params.id)
    .then(location => (
      location
        ? Promise.resolve(location)
        : Promise.reject(
          new MetaError('Not Found', {
            type: 'not found'
          })
        )
    ))
    .then((location: any) => (
      location.owned_by.toString() === req.user._id.toString()
        ? Promise.resolve(location)
        : Promise.reject(
          new MetaError('forbidden', {
            type: 'forbidden'
          })
        )
    ))
    .then(location => Locations.deleteOne({ _id: req.params.id }))
    .then((result) => res.status(204).json())
    .catch(catchAndRespond(req, res, log));
}
