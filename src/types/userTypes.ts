import { Request, Response } from 'express';
import { Document, Model } from 'mongoose';

export type UserBase = {
  avatar?:        string;
  is_active:      boolean;
  first_name:     string;
  last_name:      string;
  phone?:         string;
  username:       string;
  role:           string;
  zip_code:       string;
  tz?:            string;
};

export type UserSummary = {
  avatar:         string;
  is_active:      boolean;
  local: {
    email:        string;
  };
  first_name:     string;
  last_name:      string;
};

export type UserDetails = UserBase & {
  bgg_id?:          string;
  local?: {
    email:          string;
    password:       string;
  };
  facebook?: {
    id:             string;
    token:          string;
    email:          string;
  };
  twitter?: {
    id:             string;
    token:          string;
    email:          string;
  };
  groups?:          string[];
  game_stats?: {
    total_played:   number;
    won:            number;
    lost:           number;
  };
  password_reset?: {
    token:          string;
    expires:        Date;
  };
  tz?:              string;
};

/*
Interfaces for API methods
*/
export interface IUserDocument extends UserBase, UserDetails {
  comparePassword: (password: string) => Promise<boolean>;
  checkResetToken: (token: string) => Promise<Partial<UserDetails>>;
  setPasswordReset: () => Promise<{
    email: string;
    first_name: string;
    token: string;
  }>;
}
export interface IUserDetails extends UserBase, UserDetails {
  created_at?: Date;
}
export type IUserCreate = UserBase & Partial<UserDetails>;
export type IUserUpdate = Partial<IUserDetails>;

/* Mongoose Document & Model Types */
export interface IUserDocument extends Document, IUserDetails {}
export interface IUserModel extends Model<IUserDocument> {
  comparePassword: (password: string) => Promise<boolean>;
  checkResetToken: (token: string) => Promise<Partial<UserDetails>>;
  setPasswordReset: () => Promise<{
    email: string;
    first_name: string;
    token: string;
  }>;
  findSingleUser: (req: Request, res: Response) => Promise<Partial<UserDetails>>;
}
