import * as Joi from 'joi';
import { Request, Response } from 'express';

const schema: any = Joi.string().regex(/^[0-9a-fA-F]{24}$/);

export const validateId = (id: string) => {
  let error: any = null;

  Joi.validate(id, schema, {}, (err) => {
    error = err;
  });

  return {
    error,
    value: { _id: id }
  };
};

type RequestSource = 'body' | 'query';
export function validateRequest(resourceSchema: Joi.ObjectSchema, src: RequestSource = 'body') {
  return (req: Request, res: Response, next: () => void) => {
    console.log(req[src]);
    const { error } = resourceSchema.validate(req[src]);
    if (error) {
      return res.boom.badRequest(error.message, { meta: error.details });
    }

    next();
  };
}

export const DelimiterValidatorFactory: Joi.Extension = (joi: any) => ({
  name: 'array',
  base: joi.array(),
  coerce: function delimeterCoerce(value: string, state: any, options: any) {
    return (
      value
      ? value.split(',')
      : []
    );
  },
  rules: [{
    name: 'delimeter',
    validate: function delimiterValidate(params: any, value: any, state: any, options: any) {
      const copy = [...value];
      const validValues = copy.map(val => typeof val === 'string').sort().shift();

      // expct to see a boolean as valid values should be an array of booleans
      if (typeof validValues === 'boolean' && !validValues) {
        return this.createError('array.delimeter', {v: value, q: params.q}, state, options);
      }

      return value;
    }
  }]
});
