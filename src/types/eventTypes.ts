import { Request, Response } from 'express';
import { Document, Model } from 'mongoose';

import { IUserDetails } from './userTypes';

export type EventStatus = 'confirmed' | 'tenative' | 'cancelled';

export type EventLocation = {
  coordinates: number[];
  state: string;
  country: string;
  address_1?: string;
  address_2?: string;
  city?: string;
  postal_code?: string;
};

export type Attendee = {
  status: 'needs-action' | 'accepted' | 'declined' | 'tentative' | 'delegated';
  role: 'req-participant' | 'opt-participant';
  user: string & IUserDetails;
};

/* Describes an individual event in a listing of events */
export type EventSummary = {
  _id: string;
  attendees: string[] & IUserDetails[];
  public: boolean;
  from: number;
  to: number;
  tz: string;
  status: EventStatus;
};

/* Describes the details of an event */
export interface IParentEventDetails {
  from: number;
  to: number;
  tz: string;
  attendees: Attendee[];
  group?: string;
  organizer: string;
  loc: EventLocation;
  public: boolean;
  repeats: boolean;
  status: EventStatus;
  created_at: Date;
  update_at?: Date;
}
export type IEventCreate = {
  from: number;
  to: number;
  tz: string;
  attendees: string[] & Attendee[];
  group?: string;
  organizer: string;
  loc: EventLocation;
  public?: boolean;
  repeats?: boolean;
  status: EventStatus;
};

export type IEventUpdate = Partial<IParentEventDetails>;

/* Mongoose Document & Model Types */
export interface IEventDocument extends Document, IParentEventDetails {}
export interface IEventModel extends Model<IEventDocument> {
  createWithDetails: (payload: IEventCreate) => Promise<Partial<IParentEventDetails>>;
}
