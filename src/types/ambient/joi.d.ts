import * as Joi from 'joi';

declare module 'joi' {
  interface ObjectSchema {
    validate<T>(value: T): ValidationResult<T>
  }

  interface StringSchema {
    validate<T>(value: T): ValidationResult<T>
  }

  interface ArraySchema {
    validate<T>(value: T): ValidationResult<T>
  }
}
