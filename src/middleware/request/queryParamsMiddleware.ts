import { Request, Response, NextFunction } from 'express';
import { isEmpty } from 'lodash';

type GenericObjectType = { [key: string]: any };
interface IExtractedQueryParams {
  query: any;
  keys: string[] | {};
}

export function parseIncludes({ include }: GenericObjectType): IExtractedQueryParams {
  if (isEmpty(include)) {
    return {
      query: '',
      keys: []
    };
  }

  const includeKeys = include.split(',');
  return {
    query: (
      includeKeys.map((field: string) => ({
        path: field
      }))
    ),
    keys: includeKeys
  };
}

export function parseSparseFields({ fields, include }: GenericObjectType): IExtractedQueryParams{
  if (isEmpty(fields)) {
    return {
      query: {},
      keys: []
    };
  }

  let includeKeys: string[] = [];
  if (!isEmpty(include)) {
    includeKeys = include.split(',');
  }

  const resourceTypes = Object.keys(fields);
  const query = resourceTypes.reduce((acc: GenericObjectType, key: string) => {
    // we must do this because mongoose expects space delineated
    acc[key] = [...fields[key].split(','), ...includeKeys].join(' ');
    return acc;
  }, {});
  const typeFields = resourceTypes.reduce((acc: GenericObjectType, key: string) => {
    acc[key] = [...fields[key].split(','), ...includeKeys];
    return acc;
  }, {});

  return {
    query,
    keys: typeFields
  };
}

export function prepIncludes(req: Request, res: Response, next: NextFunction) {
  const { query, keys } = parseIncludes(req.query);
  res.locals.query = res.locals.query || {};
  res.locals.populateDBQuery = query;
  res.locals.query.parsedIncludes = keys;
  next();
}

export function prepSparseFeilds(req: Request, res: Response, next: NextFunction) {
  const { query, keys } = parseSparseFields(req.query);
  res.locals.query = res.locals.query || {};
  res.locals.fieldsDBQuery = query;
  res.locals.query.parsedFields = keys;
  next();
}
