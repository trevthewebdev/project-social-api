import { Request, Response } from 'express';
import { model, Schema } from 'mongoose';
import * as bcrypt from 'bcrypt';
import * as moment from 'moment';
import * as uuid from 'uuid/v1';

import MetaError from '../utils/errorUtils';
import { IUserDocument, IUserModel, IUserDetails } from './../types/userTypes';

const UserSchema = new Schema({
  is_active: {
    type: Boolean,
    default: true
  },
  local: {
    email: String,
    password: String
  },
  facebook: {
    id: String,
    token: String,
    email: String
  },
  twitter: {
    id: String,
    token: String,
    email: String
  },
  first_name: String,
  last_name: String,
  phone: String,
  avatar: {
    type: String,
    default: 'http://placehold.it/200'
  },
  groups: [{
    type: Schema.Types.ObjectId,
    ref: 'Group'
  }],
  game_stats: {
    total_played: {
      type: Number,
      default: 0
    },
    won: {
      type: Number,
      default: 0
    },
    lost: {
      type: Number,
      default: 0
    }
  },
  role: {
    type: String,
    default: 'user'
  },
  password_reset: {
    token: {
      type: String
    },
    expires: {
      type: Date
    }
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  zip_code: {
    type: String,
    required: true
  },
  tz: {
    type: String
  }
});

UserSchema.pre('save', function(next) {
  if (this.isModified('local.password') || this.isNew) {
    bcrypt
      .genSalt(10)
      .then(salt => bcrypt.hash(this.local.password, salt))
      .then(hash => {
        this.local.password = hash;
        return next();
      })
      .catch((error: Error) => next(error));
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function comparePassword(password: string) {
  return bcrypt.compare(password, this.local.password);
};

UserSchema.methods.checkResetToken = function checkResetToken(token: string) {
  return new Promise((resolve, reject) => {
    const tokenMatch = this.password_reset.token === token;
    const hasExpired = moment().isSameOrAfter(this.password_reset.expires);

    return (
      (!tokenMatch || hasExpired)
      ? reject(new MetaError(`invalid token or expire time: ${token}`, {
        type: 'bad request',
        token
      }))
      : resolve(this)
    );
  });
};

UserSchema.methods.setPasswordReset = function setPasswordReset() {
  return new Promise((resolve, reject) => {
    this.password_reset.token = uuid();
    this.password_reset.expires = moment().add(15, 'minutes').toISOString();

    this
      .save()
      .then(() => resolve({
        email: this.local.email,
        first_name: this.first_name,
        token: this.password_reset.token
      }))
      .catch((error: any) => reject(new MetaError(error)));
  });
};

UserSchema.statics.findSingleUser = function findSingleUser(req: Request, res: Response) {
  return (
    this
      .findById(req.params.id)
      .then((user: IUserDocument) => ({ user }))
      .catch((error: Error) => error)
  );
};

export default model<IUserDocument, IUserModel>('User', UserSchema);
