import { Request, Response, NextFunction } from 'express';
import log from '../../utils/logUtils';

type Params = { [key: string]: any };
type Operation = string | { name: string, when: (params: Params) => boolean };
type GetParams = ((req: Request, res: Response) => Params) | Params;

interface IMappedRoles {
  [roleName: string]: {
    can: {
      [operation: string]: ((params: Params) =>  boolean) | number;
    };
    inherits?: string[];
  };
}

interface IRoles {
  [roleName: string]: {
    can: (string | { name: string, when: (params: Params) => boolean })[];
    inherits?: string[];
  };
}

export default class RBAC {
  private roles: IMappedRoles = {};

  constructor(roles: IRoles) {
    this.init(roles);
  }

  public permit(operation: Operation, getParams: GetParams = {}) {
    return (req: Request, res: Response, next: NextFunction) => {
      const defaultParams: Params = { requestor: req.user };

      const resolveIncomingParams = new Promise((resolve, reject) => {
        resolve(
          (typeof getParams === 'function') ? getParams.call(getParams, req, res) : { ...getParams }
        );
      });

      resolveIncomingParams
        .then((params: Params) => ({ ...defaultParams, ...params }))
        .then((params: Params) => this.can(params.requestor.role, operation, params))
        .then(() => next())
        .catch(error => {
          log.error(error);
          return res.boom.forbidden();
        });
    };
  }

  public can(role: string, operation: Operation, params: Params): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (typeof role !== 'string') {
        throw new TypeError('Expected first parameter to be string : role');
      }

      if (typeof operation !== 'string') {
        throw new TypeError('Expected second parameter to be string : operation');
      }

      const $role = this.roles[role];
      const $operation = $role.can[operation];

      if (!$role) {
        throw new Error('Undefined role');
      }

      // if this operation is not defined at current level try what it inherited from
      if (!$role.can[operation]) {
        if (!$role.inherits) {
          return reject(false);
        }

        // return if any parent resolves true or all reject
        return (
          Promise
            .any($role.inherits.map(parent => this.can(parent, operation, params)))
            .then(result => resolve(result))
            .catch(error => reject(false))
        );
      }

      // we have the operation to resolve
      if ($operation === 1) {
        return resolve(true);
      }

      if (typeof $operation === 'function') {
        const pass = $operation(params);

        if (pass) {
          return resolve(true);
        } else {
          return reject(false);
        }
      }

      // no operation reject as false
      return reject(false);
    });
  }

  private init(roles: IRoles) {
    if (typeof roles !== 'object') {
      throw new TypeError('Expected an object as input');
    }

    const map: IMappedRoles = {};

    Object.keys(roles).forEach(role => {
      map[role] = { can: {} };

      if (roles[role].inherits) {
        map[role].inherits = roles[role].inherits;
      }

      roles[role].can.forEach((operation: Operation) => {
        if (typeof operation === 'string') {
          map[role].can[operation] = 1;
        } else if (
          typeof operation.name === 'string' &&
          typeof operation.when === 'function'
        ) {
          map[role].can[operation.name] = operation.when;
        }
      });
    });

    this.roles = map;
  }
}
