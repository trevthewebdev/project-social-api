export interface IGenericResponse {
  type: string;
  id: string;
  relationships?: object; // Fix me once we figure out how
  links?: object; // Fix me once we figure out how
}

export interface IApiResponse<T, K> {
  data: T[] | T | null;
  meta?: K;
}
