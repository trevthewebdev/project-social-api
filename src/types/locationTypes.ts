/* This file will eventually be expanded to hold different countries addresses */
import { Document, Model } from 'mongoose';

export type LocationBase = {
  name:               string;
  street:             string;
  suite?:             string;
  city:               string;
  state:              string;
  zip:                string;
};

export type LocationDetails = LocationBase & {
  lat?:               number;
  long?:              number;
  owned_by:           string;
};

export interface ILocationListing extends LocationBase {
  _id:                string;
}

export interface ILocationDetails extends LocationBase, LocationDetails {
  created_at?:        Date;
  updated_at?:        Date;
}

export type LocationCreate = LocationBase & Partial<LocationDetails> & {
  owned_by:           string;
};
export type LocationUpdate = Partial<LocationDetails>;

/* Mongoose Document & Model Types */
export interface ILocationDocument extends Document, ILocationDetails {}
export interface ILocationModel extends Model<ILocationDocument> {}
