import * as Moment from 'moment';
import * as Faker from 'faker';
import { IParentEventDetails } from './../../src/types/eventTypes';
import { staticUsers } from './userSeedData';
import { staticGroups } from './groupSeedData';

const mcKinneyKnights = staticGroups[0];

const trevor = staticUsers[0];
const shannyn = staticUsers[1];
const mark = staticUsers[2];
const luke = staticUsers[3];
const david = staticUsers[4];

export const staticEvents = [
  {
    _id: '5ad5331c581313ee712681f2',
    from: Moment('2018-05-12T19:00:00').utc().unix(),
    to: Moment('2018-05-12T23:59:59').utc().unix(),
    tz: 'America/Chicago',
    attendees: [
      { details: trevor._id },
      { details: mark._id },
      { details: luke._id }
    ],
    loc: {
      coordinates: [-96.623651, 33.275538],
      address_1: '1017 Llano Falls Dr.',
      country: 'US',
      city: 'McKinney',
      state: 'TX',
      postal_code: '75071'
    },
    organizer: trevor._id,
    group: mcKinneyKnights._id,
    public: false,
    repeats: true,
    status: 'confirmed'
  },
  // {
  //   _id: '5ad53331581313ee712681f6',
  //   attendees: [
  //     trevor._id,
  //     mark._id,
  //     luke._id
  //   ],
  //   organizer: trevor._id,
  //   public: false
  // },
  // {
  //   _id: '5ad5331e581313ee712681f3',
  //   attendees: [
  //     trevor._id,
  //     mark._id,
  //     luke._id
  //   ],
  //   organizer: trevor._id,
  //   public: true
  // },
  // {
  //   _id: '5ad5331f581313ee712681f4',
  //   attendees: [
  //     shannyn._id,
  //     david._id
  //   ],
  //   organizer: shannyn._id,
  //   public: false
  // },
  // {
  //   _id: '5ad53330581313ee712681f5',
  //   attendees: [],
  //   organizer: trevor._id,
  //   public: true
  // }
];

export default [
  ...staticEvents
];
