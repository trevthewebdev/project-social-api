// export interface IConfigEvenrionments {
//   dev:
// }

export interface IAppConfig {
  logs?: {
    level:        string;
  };
  db_name:        string;
  app_base_url:   string;
  cloudinary?: {
    cloud_name:   string;
  };
}
