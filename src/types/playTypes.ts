import { UserBase } from 'userTypes';

export type PlayBase = {
  date_played: string;
  duration: number;
  kind: string;
};

export type PlayDetails = PlayBase & {
  game: {
    _id: string;
    name: string;
  };
  players: UserBase[];
};

export type CompetitivePlayDetails = PlayBase & {
  winners: Partial<UserBase>[]; // confirm this
};

export type CoopPlayDetails = PlayBase & {
  win?: boolean;
};

export type SemiCoopPlayDetails = PlayBase & {
  winners: Partial<UserBase>[]; // confirm this
};

/*
  Interfaces for API methods
*/
export interface IPlayDetails extends PlayBase, PlayDetails {
  // created_at?: Date;
}
export type IPlayCreate = PlayBase & {
  game: string;
  players: Partial<UserBase>[]; // confirm this
};
export type IPlayUpdate = Partial<IPlayDetails>;
