import { Request, Response } from 'express';
import * as _ from 'lodash';

import logger from '../utils/logUtils';
import Groups from './../models/groupModel';
import groupModel from './../models/groupModel';

import { generateRadialQuery } from '../models/methods/geoMethods';
import {
  queryList,
  findOneResource,
  checkAlreadyExists,
  saveDocument,
  catchAndRespond
} from './methods/global';

const log = logger.child({ feature: 'locations' });

export function get(req: Request, res: Response) {
  const filter = req.query.filter || {};
  let query: any = {
    members: { $in: [req.user._id] }
  };

  if (Boolean(filter.public)) {
    query = {
      is_public: Boolean(filter.public),
      loc: {
        ...generateRadialQuery(
          parseFloat(filter.lat),
          parseFloat(filter.long),
          filter.max_miles && parseInt(filter.max_miles, 10),
          filter.min_miles && parseInt(filter.min_miles, 10)
        )
      }
    };
  }

  queryList(Groups, query, { 'loc.type': 0 })
    .then(results => {
      const groups = results[0];
      const meta = results[1];

      res.status(200).json({
        data: groups,
        meta
      });
    })
    .catch(catchAndRespond(req, res, log));
}

export function getOne(req: Request, res: Response) {
  const query = {
    _id: req.params.id,
    $or: [
      { is_public: true },
      { members: { $in: [req.user._id] } }
    ]
  };

  findOneResource(Groups, query, { 'loc.type': 0 })
    .then((group: any) => res.status(200).json({ data: group || null }))
    .catch(catchAndRespond(req, res, log));
}

export function post(req: Request, res: Response) {
  const payload = req.body;
  const { user } = req;

  findOneResource(Groups, {
    'name': payload.name,
    'loc.state': payload.state
  })
    .then(checkAlreadyExists(payload))
    .then((unsavedGroup: any) => {
      const ownerIndx = unsavedGroup.members.findIndex((id: string) => id === user._id);

      if (ownerIndx === -1) {
        unsavedGroup.members = [ unsavedGroup.owner, ...unsavedGroup.members ];
      }

      return unsavedGroup;
    })
    .then(saveDocument(Groups, log))
    .then((newGroup: any) => (
      res.status(201).json({
        data: _.omit(newGroup, ['__v'])
      })
    ))
    .catch(catchAndRespond(req, res, log));
}

export function update(req: Request, res: Response) {
  const payload = req.body;

  Groups
    .findByIdAndUpdate(req.params.id, { $set: payload }, { new: true })
    .then((result) => {
      if (!result) {
        return Promise.reject({
          type: 'not found'
        });
      }

      res.status(204).json();
    })
    .catch((err) => {
      switch (err.type) {
        case 'not found':
          res.status(404).json();
          break;

        default:
          res.boom.badImplementation(err);
      }
    });
}

export function remove(req: Request, res: Response) {
  Groups
    .findOneAndRemove({ _id: req.params.id })
    .then(result => {
      if (!result) {
        return Promise.reject({
          type: 'not found'
        });
      }

      res.status(204).json();
    })
    .catch((err) => {
      switch (err.type) {
        case 'not found':
          res.status(404).json();
          break;

        default:
          res.boom.badImplementation(err);
      }
    });
}
