import { Document, Model } from 'mongoose';
import { IGameDetails } from './gamePrimativeTypes';

// /* Mongoose Document & Model Types */
export interface IGameDocument extends Document, IGameDetails {}
export interface IGameModel extends Model<IGameDocument> {}
