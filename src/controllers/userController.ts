import * as Joi from 'joi';
import * as Mongoose from 'mongoose';
import { Request, Response } from 'express';
import * as StackParser from 'error-stack-parser';
import { MongoError } from 'mongodb';
import * as _ from 'lodash';

import log from '../utils/logUtils';

import UserModel from '../models/userModel';
import GameModel from '../models/gameModel';
import GroupModel from '../models/groupModel';

import { IUserModel, IUserDocument } from './../types/userTypes';
import { IUserCreate, IUserDetails } from './../types/userTypes';
import { validateId } from './../validators/shared/customValidators';
import {
  UserCreateSchema,
  UserUpdateSchema
} from './../validators/userValidators';

const isTesting = (process.env.NODE_ENV === 'test');

export function get(req: Request, res: Response) {
  UserModel
    .find({}, { '__v': 0, 'local.password': 0 })
    .then((users: IUserDetails[]) => {
      if (users === null || !users.length) {
        return res.boom.notFound();
      }

      return res.status(200).json({ data: users });
    })
    .catch((error: Error) => {
      log.error(error);
      return res.boom.badImplementation();
    });
}

export function getOne(req: Request, res: Response) {
  const query: any = { _id: req.params.id };
  UserModel
    .findOne(query, { '__v': 0, 'local.password': 0 })
    .exec()
    .then((user: IUserDetails) => {
      if (user === null) {
        return res.boom.notFound();
      }

      return res.status(200).json({ data: user });
    })
    .catch((error: Error) => {
      return res.boom.badImplementation();
    });
}

export function post(req: Request, res: Response) {
  const payload = req.body;

  UserModel
    .findOne({ 'local.email': payload.local.email })
    .then((existingUser) => {
      if (existingUser) {
        return Promise.reject({
          type: 'conflict',
          existingUser
        });
      }
      return Promise.resolve(payload);
    })
    .then((Payload: IUserCreate) => UserModel.create(Payload))
    .then((newUser: IUserDocument) => {
      return res.status(201).json({ data: _.pick(newUser, [
        'first_name',
        'last_name',
        'phone',
        'image',
        'local.email',
        'username'
      ]) });
    })
    .catch((err) => {
      switch (err.type) {
        case 'conflict':
          res.boom.conflict(null, {
            meta: { user_id: err.existingUser._id }
          });
          break;

        default:
          res.boom.badImplementation();
      }
    });
}

export function update(req: Request, res: Response) {
  const payload = req.body;

  UserModel
    .findByIdAndUpdate(req.params.id, { $set: payload }, { new: true })
    .then((result: any) => {
      if (result === null) {
        return res.boom.notFound();
      }
      return res.status(204).json();
    })
    .catch((err: Error) => {
      return res.boom.badImplementation();
    });
}

export function remove(req: Request, res: Response) {
  const { error } = validateId(req.params.id);

  if (error) {
    return res.boom.badRequest(error, { meta: error.details });
  }

  UserModel
    .findOneAndRemove({ _id: req.params.id })
    .then((result) => {
      if (result === null) {
        return res.boom.notFound();
      }
      return res.status(204).json();
    })
    .catch((err: Error) => {
      return res.boom.badImplementation();
    });
}
