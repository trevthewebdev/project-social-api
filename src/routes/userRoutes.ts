import * as express from 'express';

import { validateRequest } from '../validators/shared/customValidators';
import { UserCreateSchema, UserUpdateSchema } from '../validators/userValidators';

import authMiddleware from '../middleware/authMiddleware';
import { gauth } from '../config/auth';
import Users from '../models/userModel';

import {
  get,
  getOne,
  post,
  update,
  remove,
} from './../controllers/userController';

const Router = express.Router();
const auth = authMiddleware();

Router.use(auth.authenticate());

Router.get('/', gauth.permit('user:read'), get);
Router.get('/:id', gauth.permit('user:read'), getOne);
Router.post(
  '/',
  gauth.permit('user:create', req => ({ payload: req.body })),
  validateRequest(UserCreateSchema),
  post
);
Router.patch(
  '/:id',
  gauth.permit('user:update', Users.findSingleUser.bind(Users)),
  validateRequest(UserUpdateSchema),
  update
);
Router.delete('/:id', gauth.permit('user:delete'), remove);

export default Router;
