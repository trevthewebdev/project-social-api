import * as Mongoose from 'mongoose';
import { cleanDB, connectToDb } from '../db';
import log from '../src/utils/logUtils';
import { seedCollection } from '../src/utils/seedUtils';

connectToDb();

// Models
import Games from '../src/models/gameModel';
import Groups from '../src/models/groupModel';
import Locations from '../src/models/locationModel';
import Users from '../src/models/userModel';
import Events from '../src/models/eventModel';

// Schmas and Seed Data
import GameSchema from './../src/models/gameModel';
import GameSeedData from './data/gameSeedData';
import UserSchema from './../src/models/userModel';
import UserSeedData from './data/userSeedData';
import GroupSchema from './../src/models/groupModel';
import GroupSeedData from './data/groupSeedData';
import LocationSchema from './../src/models/locationModel';
import LocationSeedData from './data/locationSeedData';
import EventSchema from './../src/models/eventModel';
import EventSeedData from './data/eventSeedData';

export default (autoDisconnect: boolean) => {
  return new Promise((resolve, reject) => {
    cleanDB([
      Games,
      Users,
      Groups,
      Events,
      Locations
    ])
      .then(() => log.info('Database ready for seeding'))
      .then(seedCollection(UserSchema, UserSeedData, 'users', 'first_name'))
      .then(seedCollection(GameSchema, GameSeedData, 'games', 'name'))
      .then(seedCollection(GroupSchema, GroupSeedData, 'groups', 'name'))
      .then(seedCollection(EventSchema, EventSeedData, 'events', 'from'))
      .then(seedCollection(LocationSchema, LocationSeedData, 'locations', 'name'))
      .then(() => (autoDisconnect) ? Mongoose.disconnect() : resolve())
      .catch((err) => reject(err));
  });
};
