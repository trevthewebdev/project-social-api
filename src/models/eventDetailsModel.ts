import * as Mongoose from 'mongoose';
import { model, Schema } from 'mongoose';

import { IEventDetailsDocument, IEventDetailsModel } from '../types/eventDetailsTypes';

const EventDetailsSchema = new Mongoose.Schema({
  from: {
    $type: Number,
    required: true
  },
  to: {
    $type: Number,
    required: true
  },
  tz: {
    $type: String,
    required: true
  },
  attendees: [{
    status: {
      $type: String,
      default: 'accepted'
    },
    role: {
      $type: String,
      default: 'opt-participant'
    },
    details: {
      $type: Schema.Types.ObjectId,
      ref: 'User'
    }
  }],
  users_games: [{
    user_id: String,
    games: [{
      $type: Schema.Types.ObjectId,
      ref: 'Game'
    }]
  }],
  users_games_max: {
    $type: Number,
    min: 1,
    default: 2
  },
  parent_event: {
    $type: Schema.Types.ObjectId,
    ref: 'Event'
  },
  public: {
    $type: Boolean,
    default: false
  },
  status: {
    $type: String,
    default: 'active'
  },
  expired: {
    $type: Boolean,
    default: false
  },
  created_at: {
    $type: Date,
    require: true,
    default: Date.now
  },
  updated_at: {
    $type: Date,
    require: true,
    default: Date.now
  }
}, { typeKey: '$type' });

export default Mongoose.model<any>('EventDetails', EventDetailsSchema);
