import { Request, Response } from 'express';
import * as _ from 'lodash';

import { normalizeResponseData } from '../utils/dataUtils';
import logger from '../utils/logUtils';
import Plays, { CoopPlay } from '../models/playModel';
import { Error } from 'mongoose';

const log = logger.child({ feature: 'log play' });

export function get(req: Request, res: Response) {
  Plays
    .find({}, { __v: 0 })
    .populate({path: 'game', select: 'name' })
    .then((plays) => {
      res.status(200).json({
        data: normalizeResponseData(plays)
      });
    })
    .catch((err) => {
      log.error(err.message);
      log.debug(err.stack);
      res.boom.badImplementation(err);
    });
}

export function getOne(req: Request, res: Response) {
  // Plays
  //   .findOne(query, { __v: 0 })
  //   .then((group) => res.status(200).json({ data: group || null }))
  //   .catch((err) => res.boom.badImplementation(err));
}

export function post(req: Request, res: Response) {
  const payload = req.body;
  let Model;

  // TODO: do some request validation here

  switch (payload.type) {
    case 'coop':
      Model = CoopPlay;
      break;
    default:
      Model = Plays;
  }

  Model
    .create(payload)
    .then((newPlay: any) => {
      res.status(201).json({
        data: _.omit(newPlay.toObject(), ['__v'])
      });
    })
    .catch((err) => {
      log.error(err.message);
      log.debug(err.stack);
      res.boom.badImplementation(err);
    });
}

export function update(req: Request, res: Response) {
  const payload = req.body;

  // do some validation

  Plays
    .findByIdAndUpdate(req.params.id, { $set: payload }, { new: true })
    .then((result) => {
      if (!result) {
        return Promise.reject({
          type: 'not found'
        });
      }

      res.status(204).json();
    })
    .catch((err) => {
      switch (err.type) {
        case 'not found':
          res.status(404).json();
          break;

        default:
          log.error(err.message);
          log.debug(err.stack);
          res.boom.badImplementation(err);
      }
    });
}

export function remove(req: Request, res: Response) {
  Plays
    .findOneAndRemove({ _id: req.params.id })
    .then(result => {
      if (!result) {
        return Promise.reject({
          type: 'not found'
        });
      }

      res.status(204).json();
    })
    .catch((err) => {
      switch (err.type) {
        case 'not found':
          res.status(404).json();
          break;

        default:
          log.error(err.message);
          log.debug(err.stack);
          res.boom.badImplementation(err);
      }
    });
}
