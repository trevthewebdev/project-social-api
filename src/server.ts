import * as Express from 'express';
import * as Mongoose from 'mongoose';
import config from './config';
import { connectToDb } from './../db';
import middleware from './middleware';
import log from './utils/logUtils';
import routes from './routes/routes';

const app = Express();

app.set('env', config.env);
app.disable('views');
app.disable('view engine');
app.disable('view cache');

// Spin up global app middleware
middleware(app);

// Connect to the database
connectToDb();

// Pull in all api routes
app.use('/api/v1', routes);

// Spin up the server
app.listen(config.SERVICE_PORT || 3001, (error: Error) => {
  log.info(`Board Game API listening on http://localhost:${config.SERVICE_PORT || 3001}`);
});

export default app;
