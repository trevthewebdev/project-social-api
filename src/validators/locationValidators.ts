import * as Mongoose from 'mongoose';
import * as Joi from 'joi';
import * as _ from 'lodash';

import config from '../config';
import { Validator } from '../types/genericTypes';

import {
  LocationBase,
  LocationCreate,
  ILocationDetails,
  LocationUpdate
} from '../types/locationTypes';

// Base
const locationBaseSchemaMap: Validator<LocationBase> = {
  name: Joi.string().required(),
  street: Joi.string().required(),
  suite: Joi.string(),
  city: Joi.string().required(),
  state: Joi.string().required(),
  zip: Joi.string().required()
};

export const LocationBaseSchema = Joi.object(locationBaseSchemaMap);

// Create
const locationCreateSchemaMap: Validator<LocationCreate> = Object.assign({}, locationBaseSchemaMap, {
  lat: Joi.number(),
  long: Joi.number(),
  owned_by: Joi.string().required()
});

let LocationCreateSchemaTemp = Joi.object(locationCreateSchemaMap);
if (config.env === 'test') {
  LocationCreateSchemaTemp = LocationCreateSchemaTemp.keys({
    _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
  });
}

export const LocationCreateSchema = LocationCreateSchemaTemp;

// Update
const locationUpdateSchemaMap: Validator<LocationUpdate> = Object.assign({}, locationBaseSchemaMap, {
  name: Joi.string().optional(),
  street: Joi.string().optional(),
  city: Joi.string().optional(),
  state: Joi.string().optional(),
  zip: Joi.string().optional(),
  lat: Joi.number(),
  long: Joi.number(),
  owned_by: Joi.string().forbidden()
});

export const LocationUpdateSchema = LocationBaseSchema.keys(locationUpdateSchemaMap);

// Details
const locationDetailsSchemaMap: Validator<ILocationDetails> = Object.assign({}, locationBaseSchemaMap, {
  _id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
  lat: Joi.number(),
  long: Joi.number(),
  owned_by: Joi.string().required(),
  created_at: Joi.date().iso(),
  updated_at: Joi.date().iso()
});

export const LocationDetailsSchema = LocationBaseSchema.keys(locationDetailsSchemaMap);
