export interface IMetaError {
  message: string;
  meta?: any;
  new(message: string, meta?: any): IMetaError
}
