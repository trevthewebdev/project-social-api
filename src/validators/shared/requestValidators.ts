import * as Joi from 'joi';
import { Validator } from '../../types/genericTypes';
import { DelimiterValidatorFactory } from './customValidators';

const joi = Joi.extend(DelimiterValidatorFactory);

interface IListingQueryParams {
  include?: { [key: string]: string };
  filter?: {
    [key: string]: { [key: string]: string | number }
  };
  fields: {
    [key: string]: { [key: string]: string | number }
  };
  limit?: string | number;
  skip?: string | number;
}

export const LisingRequestSchemaMap: Validator<IListingQueryParams> = {
  filter: Joi.object().optional(),
  fields: joi.object().optional(),
  include: Joi.string().optional(),
  limit: Joi.number().max(200),
  skip: Joi.number().default(0).optional()
};
