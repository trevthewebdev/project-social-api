export const GROUP_FILTER_MAP = [
  {
    term: 'coordinates',
    path: '$near.$geometry.coordinates'
  },
  {
    term: 'public',
    path: 'is_public'
  }
];
