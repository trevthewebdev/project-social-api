import * as Chai from 'chai';
import { expect } from 'chai';
import * as _ from 'lodash';

// server & utils
import server from '../../../src/server';
import { authenticateTestUser } from '../utils/authTestUtils';
import { dropAndSeedSpecific } from '../../../src/utils/dbUtils';

// types & validators
import { IGameDetails } from '../../../src/types/gameTypes';
import { GameDetailsSchema } from '../../../src/validators/gameValidators';
// import { parent, expansion } from '../fixtures/gameFixture';

// data
import { staticUsers } from '../../../seed/data/userSeedData';
import { staticGames } from '../../../seed/data/gameSeedData';

Chai.use(require('chai-http'));

const Agent = Chai.request.agent(server);

const baseUser = staticUsers.find(({ role }) => role === 'user');
const contributorUser = staticUsers.find(({ role }) => role === 'contributor');
const adminUser = staticUsers.find(({ role }) => role === 'admin');
const superAdminUser = staticUsers.find(({ role }) => role === 'superadmin');
const evolution = staticGames.find(({ slug }) => slug === 'evolution');

describe('GET: Games', () => {
  let baseUserToken: string;
  let contributorToken: string;
  let adminToken: string;
  let superAdminToken: string;

  before(function(done: (error?: any) => void) {
    dropAndSeedSpecific(['users', 'games'])
      .then(() => Promise.all([
        authenticateTestUser(Agent, baseUser),
        authenticateTestUser(Agent, contributorUser),
        authenticateTestUser(Agent, adminUser),
        authenticateTestUser(Agent, superAdminUser)
      ]))
      .spread((bToken: string, cToken: string, aToken: string, sToken: string) => {
        baseUserToken = bToken;
        contributorToken = cToken;
        adminToken = aToken;
        superAdminToken = sToken;
        done();
      })
      .catch(error => done(error));
  });

  describe('Game Listing, filtering, & sorting: /api/v1/games', () => {
    it('should return a list of games using default settings', () => {
      return (
        Agent
          .get('/api/v1/games')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);

            expect(data).to.have.length(10);
            expect(meta).to.deep.equal({ limit: 100, skip: 0, total: 10 });

            expect(sample.attributes).to.have.property('name');
            expect(sample.attributes).to.have.property('slug');
            expect(sample.attributes).to.have.property('min_duration');
            expect(sample.attributes).to.have.property('max_duration');
            expect(sample.attributes).to.have.property('player_min');
            expect(sample.attributes).to.have.property('player_max');
          })
        );
    });

    it('should return a list of games owned by a specific user', () => {
      return (
        Agent
          .get(`/api/v1/games?filter[owned_by]=${superAdminUser._id}`)
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);
            expect(sample.attributes).to.have.property('owned_by');
            expect(sample.attributes.owned_by.indexOf(superAdminUser._id)).to.not.equal(-1);
          })
      );
    });

    it('should return a list of games when using min_duration', () => {
      return (
        Agent
          .get('/api/v1/games?filter[min_duration]=90')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(data.length).to.be.greaterThan(0);
            expect(sample.attributes.min_duration).to.be.gte(90);
          })
      );
    });

    it('should return a list of games with a max_duration', () => {
      return (
        Agent
          .get('/api/v1/games?filter[max_duration]=60')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(data.length).to.be.greaterThan(0);
            expect(sample.attributes.max_duration).to.be.lte(60);
          })
      );
    });

    it('should return a list of games with min/max duration range', () => {
      return (
        Agent
          .get('/api/v1/games?filter[min_duration]=90&filter[max_duration]=240')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);
            expect(data.length).to.be.greaterThan(0);
            expect(sample.attributes.min_duration).to.be.gte(90);
            expect(sample.attributes.max_duration).to.be.lte(240);
          })
      );
    });

    it('should return a list of games with a min player count', () => {
      return (
        Agent
          .get('/api/v1/games?filter[player_min]=2')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);
            expect(data.length).to.be.greaterThan(0);
            expect(sample.attributes.player_min).to.be.gte(2);
          })
      );
    });

    it('should return a list of games with a max player count', () => {
      return (
        Agent
          .get('/api/v1/games?filter[player_max]=5')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);
            expect(data.length).to.be.greaterThan(0);
            expect(sample.attributes.player_max).to.be.lte(5);
          })
      );
    });

    it('should return a list of games with a specific mechanic', () => {
      return (
        Agent
          .get('/api/v1/games?filter[mechanics]=dice_rolling&fields[games]=primary_mechanic')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);
            expect(data.length).to.greaterThan(0);
            expect(sample.attributes.primary_mechanic).to.eql('dice_rolling');
          })
      );
    });

    it('it should return only expansions', () => {
      return (
        Agent
          .get('/api/v1/games?filter[is_expansion]=true')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);
            expect(data.length).to.greaterThan(0);
            expect(sample.attributes.is_expansion).to.eql(true);
          })
      );
    });

    it('it should not populate related resources when only specified as a sparse field', () => {
      return (
        Agent
          .get(`/api/v1/games?fields[games]=owned_by&filter[owned_by]=${superAdminUser._id}`)
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(sample.attributes.owned_by)).to.equal(true);
            expect(typeof sample.attributes.owned_by[0]).to.equal('string');
            expect(sample.attributes.owned_by.indexOf(superAdminUser._id)).to.not.equal(-1);
          })
      );
    });

    it('it should return included resources without specifying them as sparse fields', () => {
      return (
        Agent
          .get(`/api/v1/games?filter[owned_by]=${superAdminUser._id}&include=parent_game,owned_by`)
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, included } = response.body;
            const includedUser = included.find((inc: any) => inc.type === 'users');
            const includedGame = included.find((inc: any) => inc.type === 'games');

            expect(response).to.have.property('status', 200);
            expect(includedUser).to.have.property('id');
            expect(includedUser.attributes).to.have.property('first_name');
            expect(includedGame).to.have.property('id');
            expect(includedGame.attributes).to.have.property('min_duration');
          })
      );
    });

    it('it should return a sparse fieldset of included resources', () => {
      return (
        Agent
          .get(`/api/v1/games?filter[owned_by]=${superAdminUser._id}&include=owned_by&fields[users]=first_name,last_name,local.email`)
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, included } = response.body;
            const includedUser = included.find((inc: any) => inc.type === 'users');

            expect(response).to.have.property('status', 200);
            expect(includedUser).to.have.property('id');
            expect(includedUser.attributes).to.have.property('first_name', 'Trevor');
            expect(includedUser.attributes).to.have.property('last_name', 'Pierce');
            expect(includedUser.attributes.local).to.have.property('email', 'trevthewebdev@gmail.com');
          })
      );
    });

    it('should return an empty array with correct meta data if no game was found', () => {
      return (
        Agent
          .get('/api/v1/games?filter[min_duration]=1000')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .then(response => {
            const { data, meta } = response.body;

            expect(response).to.have.property('status', 200);
            expect(data).to.equal(null);
            expect(meta).to.deep.equal({ limit: 100, skip: 0, total: 0 });
          })
      );
    });

    it('should fail with 400 with invalid query params', () => {
      return (
        Agent
          .get('/api/v1/games?filter[yomomma]=goestocollege')
          .set('Authorization', `Bearer ${superAdminToken}`)
          .catch(error => {
            expect(error).to.have.property('status', 400);
            expect(error.response.body).to.have.property('error', 'Bad Request');
            expect(error.response.body).to.have.property(
              'message',
              'child "filter" fails because ["yomomma" is not allowed]'
            );
          })
      );
    });
  });

  xdescribe('Getting Individual Games: /api/v1/games/:id', () => {
    it('should return a single game by :id', done => {
      return Agent
        .get(`/api/v1/games/${evolution._id}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .then(response => {
          expect(response).to.have.status(200);
          expect(response.body.data).to.have.property('name', 'Evolution');
          expect(response.body.data).to.have.property('slug', 'evolution');
          done();
        })
        .catch(error => done(error));
    });

    it('should return a single game by :slug', done => {
      return Agent
        .get(`/api/v1/games/${evolution.slug}`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .then(response => {
          expect(response).to.have.status(200);
          expect(response.body.data).to.have.property('name', 'Evolution');
          expect(response.body.data).to.have.property('slug', 'evolution');
          done();
        })
        .catch(error => done(error));
    });

    it('should fail with 404 to return a game if it\'s not found', done => {
      return Agent
        .get(`/api/v1/games/nevergonnafindthisgame`)
        .set('Authorization', `Bearer ${superAdminToken}`)
        .catch(error => {
          expect(error).to.have.status(404);
          done();
        });
    });
  });

  describe('Game Listing Permissions / Roles', () => {
    it('should allow the user role to access games', () => {
      return (
        Agent
          .get('/api/v1/games')
          .set('Authorization', `Bearer ${baseUserToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);

            expect(data).to.have.length(10);
            expect(meta).to.deep.equal({ limit: 100, skip: 0, total: 10 });

            expect(sample.attributes).to.have.property('name');
            expect(sample.attributes).to.have.property('slug');
            expect(sample.attributes).to.have.property('min_duration');
            expect(sample.attributes).to.have.property('max_duration');
            expect(sample.attributes).to.have.property('player_min');
            expect(sample.attributes).to.have.property('player_max');
          })
      )
    });

    it('should allow the contributor role to access games', () => {
      return (
        Agent
          .get('/api/v1/games')
          .set('Authorization', `Bearer ${contributorToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);

            expect(data).to.have.length(10);
            expect(meta).to.deep.equal({ limit: 100, skip: 0, total: 10 });

            expect(sample.attributes).to.have.property('name');
            expect(sample.attributes).to.have.property('slug');
            expect(sample.attributes).to.have.property('min_duration');
            expect(sample.attributes).to.have.property('max_duration');
            expect(sample.attributes).to.have.property('player_min');
            expect(sample.attributes).to.have.property('player_max');
          })
      );
    });

    it('should allow the admin role to access games', () => {
      return (
        Agent
          .get('/api/v1/games')
          .set('Authorization', `Bearer ${adminToken}`)
          .then(response => {
            const { data, meta } = response.body;
            const sample = _.sample(data);

            expect(response).to.have.property('status', 200);
            expect(_.isArray(data)).to.equal(true);

            expect(data).to.have.length(10);
            expect(meta).to.deep.equal({ limit: 100, skip: 0, total: 10 });

            expect(sample.attributes).to.have.property('name');
            expect(sample.attributes).to.have.property('slug');
            expect(sample.attributes).to.have.property('min_duration');
            expect(sample.attributes).to.have.property('max_duration');
            expect(sample.attributes).to.have.property('player_min');
            expect(sample.attributes).to.have.property('player_max');
          })
      );
    });
  });

  xdescribe('Individual Games Permissions / Roles', () => {
    it('should allow the user role to access a single game', done => {
      return Agent
        .get(`/api/v1/games/${evolution._id}`)
        .set('Authorization', `Bearer ${baseUserToken}`)
        .then(response => {
          expect(response).to.have.status(200);
          expect(response.body.data).to.have.property('name', 'Evolution');
          expect(response.body.data).to.have.property('slug', 'evolution');
          done();
        })
        .catch(error => done(error));
    });

    it('should allow the contributor role to access a single game', done => {
      return Agent
        .get(`/api/v1/games/${evolution._id}`)
        .set('Authorization', `Bearer ${contributorToken}`)
        .then(response => {
          expect(response).to.have.status(200);
          expect(response.body.data).to.have.property('name', 'Evolution');
          expect(response.body.data).to.have.property('slug', 'evolution');
          done();
        })
        .catch(error => done(error));
    });

    it('should allow the admin role to access a single game', done => {
      return Agent
        .get(`/api/v1/games/${evolution._id}`)
        .set('Authorization', `Bearer ${adminToken}`)
        .then(response => {
          expect(response).to.have.status(200);
          expect(response.body.data).to.have.property('name', 'Evolution');
          expect(response.body.data).to.have.property('slug', 'evolution');
          done();
        })
        .catch(error => done(error));
    });
  });
});
