# -- base --
FROM node:8 AS base
# set app working directory (in the container)
WORKDIR /usr/src/overture-api
# copy package file
COPY package*.json ./

# -- dependencies --
FROM base AS dependencies
# install ALL node_modules, include dev modules
RUN npm install
COPY . .
EXPOSE 3001

CMD [ "npm", "start" ]