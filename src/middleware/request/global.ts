import * as uuid from 'uuid/v1';
import { Request, Response, NextFunction } from 'express';

export function attachRequestId(req: any, res: Response, next: NextFunction) {
  let newId;
  const existingId = req.headers['RequestId'];

  if (existingId) {
    req.req_id = existingId;
    res.setHeader('RequestId', existingId);
    return next();
  }

  newId = uuid();
  req.req_id = newId;
  req.headers['RequestId'] = newId;
  res.setHeader('RequestId', newId);
  next();
}
