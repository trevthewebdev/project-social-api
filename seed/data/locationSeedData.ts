import * as _ from 'lodash';
import * as Faker from 'faker';

import config from '../../src/config';
import { ILocationDetails } from './../../src/types/locationTypes';
import { staticUsers } from './userSeedData';
import { slugify } from '../../src/utils/requestUtils';

// remove the 4th static user from the array for tests
const seedUserIds = staticUsers.map(seedUser => seedUser._id);

function generateLocations(count: number) {
  const locations: Partial<ILocationDetails>[] = [];
  while (--count) {
    const name: string = Faker.random.words();
    locations.push({
      name,
      street: Faker.address.streetAddress(),
      city: Faker.address.city(),
      state: Faker.address.stateAbbr(),
      zip: Faker.address.zipCode(),
      lat: Faker.address.latitude(),
      long: Faker.address.longitude(),
      owned_by: _.sample(seedUserIds)
    });
  }
  return locations;
}

export const staticLocations = [
  {
    _id: '5a8f9f92bbf044c290f2ea28',
    name: 'Madness Games and Comics',
    street: '3000 Custer Rd',
    suite: '310',
    city: 'Plano',
    state: 'tx',
    zip: '75075',
    lat: 33.039783,
    long: -96.732162,
    owned_by: seedUserIds[2]
  },
  {
    _id: '5a9040b7bbf044c290f2ea29',
    name: 'My House',
    street: '1017 Llano Falls Dr',
    city: 'McKinney',
    state: 'tx',
    zip: '75071',
    lat: 33.275538,
    long: -96.623651,
    owned_by: seedUserIds[0]
  },
  {
    _id: '5a90a14abbf044c290f2ea2a',
    name: 'My House',
    street: '2824 Chelsea Ln',
    city: 'Flower Mound',
    state: 'tx',
    zip: '75028',
    lat: 33.043811,
    long: -97.073790,
    owned_by: seedUserIds[1]
  },
  {
    _id: '5a90a538bbf044c290f2ea2b',
    name: 'Common Ground Games',
    street: '1328 Inwood Rd',
    city: 'Dallas',
    state: 'tx',
    zip: '75247',
    lat: 32.805896,
    long: -96.852000,
    owned_by: seedUserIds[3]
  }
];

export const testLocation1 = {
  _id: '5a919944bbf044c290f2ea2c',
  name: 'Comic Store',
  street: '2787 Preston Rd',
  suite: '1120',
  city: 'Frisco',
  state: 'tx',
  zip: '75034',
  lat: 33.102363,
  long: -96.807364,
  owned_by: seedUserIds[0]
};

export const testLocation2 = {
  _id: '5a91994bbbf044c290f2ea2d',
  name: 'Comic Store',
  street: '1681 N Central Expy',
  suite: '300',
  city: 'McKinney',
  state: 'tx',
  zip: '75070',
  lat: 33.214529,
  long: -96.638092,
  owned_by: seedUserIds[1]
};

export default [
  ...staticLocations,
  ...generateLocations(config.env === 'test' ? 5 : 15)
];
