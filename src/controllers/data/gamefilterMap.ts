export const GAME_FILTER_MAP = [
  {
    term: 'owned_by',
    operator: '$in',
    path: 'owned_by'
  },
  {
    term: 'min_duration',
    operator: '$gte',
    path: 'min_duration'
  },
  {
    term: 'max_duration',
    operator: '$lte',
    path: 'max_duration'
  },
  {
    term: 'player_min',
    operator: '$gte',
    path: 'player_min'
  },
  {
    term: 'player_max',
    operator: '$lte',
    path: 'player_max'
  },
  {
    term: 'player_best',
    path: 'player_best'
  },
  {
    term: 'mechanics',
    operator: '$all',
    path: 'mechanics'
  },
  {
    term: 'min_weight',
    operator: '$gte',
    path: 'weight'
  },
  {
    term: 'max_weight',
    operator: '$lte',
    path: 'weight'
  },
  {
    term: 'theme',
    path: 'theme'
  },
  {
    term: 'is_expansion',
    path: 'is_expansion'
  }
];

export const GAME_SORT_MAP = [
  {
    term: 'player_min',
    path: 'details.player_min'
  },
  {
    term: 'player_max',
    path: 'details.player_max'
  },
  {
    term: 'total_plays',
    path: 'stats.total_plays'
  },
  {
    term: 'weight',
    path: 'details.weight'
  }
];
