export const user1 = {
  _id: '5a8f9f76bbf044c290f2ea27',
  local: {
    email: 'themark@gmail.com',
    password: 'password'
  },
  first_name: 'Mark',
  last_name: 'Hagood',
  zip_code: '75075',
  username: 'themarkhagood'
};

export const user2 = {
  _id: '5934d9f497377999f29115c7',
  local: {
    email: 'trevor@gmail.com',
    password: 'password'
  },
  first_name: 'Trevor',
  last_name: 'Pierce',
  zip_code: '75071',
  username: 'thecave'
};

export const newAdminUser = {
  _id: '5abdc3dfa70a874a70b3a143',
  first_name: 'New',
  last_name: 'Admin',
  local: {
    email: 'newadmin@thenewestadmin.com',
    password: 'password'
  },
  zip_code: '75071',
  username: 'anadmin',
  role: 'admin'
};
