/* @FIXME: move everything below here somewhere else */
import * as _ from 'lodash';
import { Request, Response, NextFunction } from 'express';
import log from '../../utils/logUtils';
import MetaError from '../../utils/errorUtils';

import { generateRadialQuery } from '../../models/methods/geoMethods';

export function setFilters(FILTER_MAP: { [key: string]: string }[]) {
  return (req: Request, res: Response, next: NextFunction) => {
    log.debug('::setFilters:: req.query', req.query);
    const filters = req.query.filter || {};

    if (_.isEmpty(filters)) {
      next();
      return; // @FIXME: this is a coverup for another bug below where headers were being set
    }

    buildMongoQuery(FILTER_MAP, filters)
      .then((dbQuery: any) => {
        res.locals = _.extend({}, res.locals, {
          filterDBQuery: dbQuery
        });
        next();
      })
      .catch((error: any) => {
        res.status(400).json({
          errors: [{
            title: error.message,
            meta: error.meta
          }]
        });
      });
  };
}

// retreives a single part of a query: { min_duration: { $gte: 3 } }
type MongoOperator = { [key: string]: any };
export function getQueryForTerm(schema: any, key: any, value: any) {
  const { operator }: MongoOperator = _.find(schema, { term: key });
  const subQuery: any = {};

  if (!operator) {
    return value;
  }

  switch (operator) {
    case '$all':
    case '$in':
      subQuery[operator] = value.split(',');
      break;
    default:
      subQuery[operator] = value;
  }

  return subQuery;
}

// builds a full mongo query given a set of query terms sent in from query params
export function buildMongoQuery(backendSchema: any, filterParams: any) {
  const { lat, long, max_miles, min_miles } = filterParams;
  const modifiedParams = Object.assign({}, filterParams);
  let query: any = {};

  // FIXME: something seems wrong with this
  if (lat && long) {
    query = Object.assign({}, query, generateRadialQuery(
      parseFloat(lat),
      parseFloat(long),
      max_miles,
      min_miles
    ));

    delete modifiedParams.lat;
    delete modifiedParams.long;
    delete modifiedParams.max_miles;
    delete modifiedParams.min_miles;
  }

  return new Promise((resolve, reject) => {
    for (const key in modifiedParams) {
      if (modifiedParams.hasOwnProperty(key)) {
        const scheme = _.find(backendSchema, { term: key });
        // Bail if we can't find the term in the backend schema
        if (!scheme) {
          return reject(new MetaError(`unrecognized key: ${key}`, { key }));
        }

        const { path }: any = scheme;
        const newPart: any = getQueryForTerm(backendSchema, key, modifiedParams[key]);

        if (query[path]) {
          query[path] = _.extend({}, query[path], newPart);
        } else {
          query[path] = newPart;
        }

        log.debug('Built query', query);
      }
    }

    return resolve(query);
  });
}
