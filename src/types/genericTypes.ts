import * as Joi from 'joi';

export type Validator<T> = {
  [K in keyof T]: Joi.Schema;
};
